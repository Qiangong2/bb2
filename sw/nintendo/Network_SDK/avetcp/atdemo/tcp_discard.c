/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

#define DISCARD_PORT 9

#define MESNUM 10
static OSMessageQueue asrEventQueue;
static OSMessage asrEventMessage[MESNUM];
static AT_VOID asr(AT_SINT16 nh, AT_SINT16 event)
{

	OSReport("asr nh %d event %d\n", nh, event);

	OSSendMessage(&asrEventQueue, (OSMessage*)event, OS_MESSAGE_NOBLOCK);

}

static AT_SINT16 td_nh;
static AT_UINT32 td_sent;
static int td_abort = 0;
static void* tcp_discard_send_thread(void* arg)
{
	AT_SINT16 ret;
	AT_SINT16 nh;
	AT_IP_ADDR *addr;
	AT_SEND_BUFFS buff;
	AT_SINT16 event;
	OSMessage msg;

	addr = (AT_IP_ADDR*)arg;
	
	ret = tcp_create();
	if (ret < 0) {
		goto tcp_discard_send_exit;
	}
	nh = ret;
	td_nh = ret;
	OSInitMessageQueue(&asrEventQueue, asrEventMessage, sizeof(asrEventMessage) / sizeof(asrEventMessage[0]));
	set_asr(nh, asr);
	ret = tcp_connect(nh, addr, AT_HTONS(DISCARD_PORT), AT_NULL);
	if (ret < 0) {
		goto tcp_discard_send_delete;
	}
	OSReceiveMessage(&asrEventQueue, &msg, OS_MESSAGE_BLOCK);
	event = (AT_SINT16)msg;
	if (event != AT_ASR_ESTABLISHED) {
		goto tcp_discard_send_abort;
	}

	for (;;) {
		if (td_abort) {
			goto tcp_discard_send_abort;
		}
		buff.buff = send_buf;
		buff.len = SEND_BUF_SIZE;
		ret = tcp_send(nh, AT_NULL, 1, &buff);
		if (ret < 0) {
			OSReport("tcp_send(%d) ret %d\n", nh, ret);
			break;
		}
		td_sent += SEND_BUF_SIZE;
	}

tcp_discard_send_abort:
	ret = tcp_abort(nh);
	OSReport("tcp_abort(%d) ret %d\n", nh, ret);
tcp_discard_send_delete:
	ret = tcp_delete(nh);
	OSReport("tcp_delete(%d) ret %d\n", nh, ret);
tcp_discard_send_exit:
	OSReport("tcp_discard_recv_thread end\n");
	td_abort = 0;

	return (NULL);
}

static void stop_thread(void)
{
	AT_SINT16 ret;

	td_abort = 1;
	set_asr(td_nh, AT_NULL);
	OSSendMessage(&asrEventQueue, (OSMessage*)(-1), OS_MESSAGE_NOBLOCK);
	ret = tcp_cancel(td_nh, 1); /* interrupt tcp_send() */
	OSReport("tcp_cancel(nh = %d, SEND) ret %d\n", td_nh, ret);
}

static OSThread td_th;
#define TD_STACKSIZE 4096
static AT_UBYTE td_stack[TD_STACKSIZE];
s16 tcp_discard_send_test(s32 abort_flag) 
{
	static int phase = 0;
	static int wait = 0;
	static u32 s_frame;
	static u32 d_frame;
	static u32 sec;
	static u32 thru;
	static AT_IP_ADDR addr;
	AT_SINT16 ret;
	s16 flag;
	enum {
		phase_dns = 0,
		phase_create_thread,
		phase_abort_wait,
		phase_end
	};

	flag = 0;
	if (abort_flag) {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(AT_NULL, AT_NULL, 1);
			break;
		case phase_create_thread:
			break;
		case phase_abort_wait:
			stop_thread();
			break;
		case phase_end:
			break;
		}
		flag = 1;
	} else {
		switch (phase) {
		case phase_dns:
			ret = dns_get_addr_proc(TEST_SERVER_NAME, &addr, abort_flag);
			if (ret == 1) {
				phase = phase_create_thread;
			} else if (ret == -1) {
				flag = 1;
			}
			break;
			
		case phase_create_thread:
			OSCreateThread(&td_th, tcp_discard_send_thread, (void*)&addr, td_stack + TD_STACKSIZE, TD_STACKSIZE, 16, OS_THREAD_ATTR_DETACH);
			OSResumeThread(&td_th);
			s_frame = gFrame;
			phase = phase_abort_wait;
			break;

		case phase_abort_wait:
			DEMOPrintf(x, y += 16, 0, "peer %s (%s)", TEST_SERVER_NAME, nint2ipdot(addr.v4addr));
			DEMOPrintf(x, y += 16, 0, "send %u bytes", td_sent);
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> stop");

			if (OSIsThreadTerminated(&td_th) == TRUE) {
				flag = 1;
				break;
			}
			if (gButton & PAD_BUTTON_A) {
				stop_thread();
				d_frame = gFrame - s_frame;
				sec = d_frame / 60;
				thru = td_sent / sec;
				phase = phase_end;
			}
			break;

		case phase_end:
			DEMOPrintf(x, y += 16, 0, "%u sec %u bytes/sec", sec, thru);
			if (gButton & PAD_BUTTON_A) {
				flag = 1;
			}
			break;
		}
	}
	if (flag) {
		phase = 0;
		wait = 0;
		td_sent = 0;
	}
	return (flag);
}

