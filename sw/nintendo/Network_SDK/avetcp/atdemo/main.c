/*
 * AVE-TCP for Nintendo GameCube Sample Program
 *
 */

#include <stdlib.h>
#include <string.h>
#include <demo.h>

#include "avetcp.h"
#include "atdemo.h"

s16 gDeviceType = DEV_UNKNOWN;
s16 gIpSetting = -1;
AT_IF_INFO gIfinfo;
AT_UBYTE gMacaddr[6];
AT_NINT32 gGateway = 0;
AT_IP_ADDR gDns1;
AT_IP_ADDR gDns2;
AT_ROUTE4_LIST gRouteList[26];
AT_SINT16 gRouteNum = 0;
AT_PPP_MODEM_PROPERTY gPmprop;
AT_PPP_START_ARG gPsarg;
AT_PPP_STAT_ARG gPstat;
AT_SINT16 gDhcpStat = DHCP_STATE_INIT;
AT_UBYTE recv_buf[RECV_BUF_SIZE];
AT_UBYTE send_buf[SEND_BUF_SIZE];
u16 gButton;
s16 x, y;
u32 gFrame;

static AT_SINT16 gArpProbeResult;
static AT_UBYTE gArpProbeMac[6];
static PADStatus Pads[PAD_MAX_CONTROLLERS];
extern const char * const AveTcpGcBuild;

static void tcpproc(void);
static s16 selectIpSetting(void);
static s16 selectTest(void);
static void set_pmprop(AT_PPP_MODEM_PROPERTY* pmprop);
static void set_psarg(AT_PPP_START_ARG* psarg, s32 pppoe);
static AT_VOID ppp_connect_notify(AT_SINT32 result, AT_SINT32 speed);
static AT_VOID arp_probe_notify (AT_SINT16 result, AT_UBYTE *mac);

void main()
{
	s32 chan;
	u32 padBit;
	u32 resetBits;
	u32 connectedBits;
	GXRenderModeObj* rmp;
	GXColor blue = {0, 0, 127, 0};
	s32 result;
	u32 type;
	u16 curbtn = 0;
	u16 oldbtn = 0;
	u8* device_type_str;

	gButton = 0;
	x = 0;
	y = 0;
	gFrame = 0;
	
	DEMOInit(NULL);
	
	OSReport ( "%s\n", AveTcpGcBuild);
	
	connectedBits = 0;
	GXSetCopyClear(blue, 0x00ffffff);
	GXCopyDisp(DEMOGetCurrentBuffer(), GX_TRUE);
	rmp = DEMOGetRenderModeObj();
	DEMOInitCaption(DM_FT_XLU, (s16)rmp->fbWidth * 2 / 3, (s16)rmp->efbHeight * 2 / 3);

	/* Decide device type */
	result = EXIGetType(0, 2, &type);
	OSReport("EXIGetType: result %d type %08x\n", result, type);
	switch (type) {
		case EXI_ETHER:
			gDeviceType = DEV_BBA;
			device_type_str = (u8*)DEV_BBA_STR;
			break;
		case EXI_MODEM:
			gDeviceType = DEV_MODEM;
			device_type_str = (u8*)DEV_MODEM_STR;
			break;
		default:
			gDeviceType = DEV_UNKNOWN;
			device_type_str = (u8*)DEV_UNKNOWN_STR;
			break;
	}

	for (;;) {
		DEMOBeforeRender();
		x = 10;
		y = 0;
		gFrame++;
		
		PADRead(Pads);
		resetBits = 0;
		for (chan = 0; chan < PAD_MAX_CONTROLLERS; ++chan) {
			padBit = PAD_CHAN0_BIT >> chan;
			switch (Pads[chan].err) {
				case PAD_ERR_NONE:
				case PAD_ERR_TRANSFER:
					connectedBits |= padBit;
					break;
				case PAD_ERR_NO_CONTROLLER:
					resetBits |= padBit;
					break;
				case PAD_ERR_NOT_READY:
				default:
					break;
			}
		}
		if (connectedBits) {
			resetBits &= connectedBits;
		}
		if (resetBits) {
			PADReset(resetBits);
		}
		curbtn = Pads[0].button;
		gButton = (u16)((curbtn ^ oldbtn) & curbtn);
		oldbtn = curbtn;

		DEMOPrintf(x, y += 16, 0, "AVE-TCP for GAMECUBE DEMO %d", gFrame);
		DEMOPrintf(x, y += 16, 0, "DEVICE [%s]", device_type_str);

		tcpproc();

        DEMODoneRender();
	}
}

enum {
	phase_init = 0,
	phase_dl_init,
	phase_dl_start,
	phase_dl_start_wait,
	phase_if_config,

	phase_test_select,
	phase_test,

	phase_if_down,
	phase_dl_stop,
	phase_dl_stop_wait,
	phase_dl_term,

	phase_restart_wait,

	phase_term,
	phase_end,

	phase_assert
};

static void tcpproc(void)
{
	static s32 phase = phase_init;
	static AT_UBYTE* workarea;
	static AT_SINT16 if_num_ppp = -1;
	static AT_SINT16 if_num_dix = -1;
	static AT_SINT16 if_num = -1;
	static s32 wait = 0;
	static s32 stat_lookup_wait = 0;
	static s16 test;
	static AT_VOID* dump_buf;
	static AT_UINT32 dump_data_bytes;
	static AT_UINT32 dump_packet_counts;
	static s32 abort = 0;
	AT_SINT16 ret;
	
	DEMOPrintf(x, y += 16, 0, "PHASE %d", phase);

	if (phase >= phase_if_config && phase < phase_if_down) {
		/* Monitor circuit condition */
		if (gDeviceType == DEV_BBA) {
			if (AT_LinkUpStatus() == 0) {
				abort = 1;
			}
		}
		if (gDeviceType == DEV_MODEM ||
			(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
			if (stat_lookup_wait == 0) {
				ret = ppp_stat(&gPstat);
				if (gPstat.state != AT_PPP_STAT_ESTABLISHED) {
					abort = 1;
				}
				stat_lookup_wait = 60;
			} else {
				stat_lookup_wait--;
			}
			print_ppp_stat(gPstat.state);
		} else if (gIpSetting == IP_SETTING_DHCP) {
			if (stat_lookup_wait == 0) {
				gDhcpStat = (AT_SINT16)DHCP_timer();
				if (gDhcpStat == DHCP_STATE_TIMEOUT || gDhcpStat == DHCP_STATE_ERROR) {
					abort = 1;
				}
				stat_lookup_wait = 60;
			} else {
				stat_lookup_wait--;
			}
			print_dhcp_stat(gDhcpStat);
		}
	}
	if (gButton & PAD_BUTTON_B) {
		abort = 1;
	}

	switch (phase) {
		case phase_init:
#ifdef AT_GC_DEBUG
			AT_SetDebugFlag(AT_DPLEVEL_PPP | AT_DPLEVEL_PPPOE);
#endif /* AT_GC_DEBUG */
			ret = (AT_SINT16)AT_SetTaskPriority(16);

			/* Allocate TCP work area */
			{
				AT_INIT_PARAM para;
				AT_SINT32 size;

				size = AT_GetInitParam(&para); /* Get default parameter */

				para.tcp_port_num = TCP_PORT_NUM; /* Overwrite parameter you wish to change */
#ifdef AT_GC_DEBUG
				para.dump_buffer_size = 1000000;
#endif /* AT_GC_DEBUG */
				
				size = AT_SetInitParam(&para); /* Reset parameter, get necessary area size */
				if (size < 0) {
					phase = phase_assert;
					break;
				}

				workarea = (AT_UBYTE*)OSAlloc((u32)size); 
				if (workarea) {
					ret = AT_SetWorkArea(workarea, size); /* Give secured area to TCPLib */
					if (ret < 0) {
						phase = phase_assert;
						break;
					}
				} else {
					phase = phase_assert;
					break;
				}
			}
			ret = avetcp_init();
			OSReport("avetcp_init ret %d\n", ret);
			if (ret < 0) {
				phase = phase_assert;
			} else {
				phase = phase_dl_init;
			}
			break;

		case phase_dl_init:
#ifdef AT_GC_DEBUG
			ret = AT_PacketDumpStart(100, 0);
			if (ret < 0) {
				phase = phase_term;
			}
#endif /* AT_GC_DEBUG */
			ret = dns_init();
			if (ret < 0) {
				phase = phase_term;
				break;
			}
			switch (gDeviceType) {
				case DEV_BBA:
					ret = ppp_init();
					if (ret < 0) {
						dns_term();
						phase = phase_term;
						break;
					} 
					if_num_ppp = ret;
					
					ret = arp_init();
					if (ret < 0) {
						dns_term();
						ppp_term();
						if_num_ppp = -1;
						phase = phase_term;
						break;
					}

					ret = dix_init();
					if (ret < 0) {
						dns_term();
						ppp_term();
						if_num_ppp = -1;
						phase = phase_term;
						break;
					}
					if_num_dix = ret;
					phase = phase_dl_start;
					break;

				case DEV_MODEM:
					ret = ppp_init();
					if (ret < 0) {
						dns_term();
						phase = phase_term;
						break;
					}
					if_num_ppp = ret;
					phase = phase_dl_start;
					break;

				default:
					dns_term();
					phase = phase_term;
					break;
			}		
			break;

		case phase_dl_start:
			if (gDeviceType == DEV_BBA) {
				gIpSetting = selectIpSetting();
				if (gIpSetting < 0) {
					break;
				}
			}
			if (gDeviceType == DEV_MODEM ||
				(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
				s16 pppoe;

				if_num = if_num_ppp;
				set_pmprop(&gPmprop);
				ppp_set_modem_property(&gPmprop);
				pppoe = (s16)((gDeviceType == DEV_BBA) ? 1 : 0);
				set_psarg(&gPsarg, pppoe);
				ret = ppp_start(&gPsarg);
				if (ret < 0) {
					phase = phase_dl_term;
				}
				phase = phase_dl_start_wait;

			} else if (gIpSetting == IP_SETTING_DHCP) {
				if_num = if_num_dix;

				ret = (AT_SINT16)DHCP_init(if_num);
				if (ret < 0) {
					phase = phase_dl_term;
					break;
				}
				ret = (AT_SINT16)DHCP_hostname((AT_UBYTE*)DHCP_HOSTNAME);
				if (ret < 0) {
					DHCP_terminate();
					phase = phase_dl_term;
					break;
				}
				gIfinfo.type = AT_IPv4;
				ret = (AT_SINT16)DHCP_request_nb(0, 0, 0, AT_NULL,
						&gIfinfo.v4address, &gIfinfo.v4netmask, &gIfinfo.v4brc, 30);
				if (ret < 0) {
					DHCP_terminate();
					phase = phase_dl_term;
					break;
				}
				phase = phase_dl_start_wait;

			} else if (gIpSetting == IP_SETTING_MANUAL) {
				if_num = if_num_dix;

				gIfinfo.type = AT_IPv4;
				gIfinfo.v4address = AT_HTONL(MY_IPADDR);
				gIfinfo.v4netmask = AT_HTONL(MY_NETMASK);
				gIfinfo.v4brc = AT_HTONL(0);
				gGateway = AT_HTONL(MY_GATEWAY);
				gDns1.type = AT_IPv4;
				gDns1.v4addr = AT_HTONL(MY_DNS1);
				gDns2.type = AT_IPv4;
				gDns2.v4addr = AT_HTONL(MY_DNS2);

				gArpProbeResult = AT_API_PENDING;
				ret = arp_probe (if_num, gIfinfo.v4address, 5000, arp_probe_notify);
				if (ret != AT_API_PENDING) {
					phase = phase_dl_term;
					break;
				}
				phase = phase_dl_start_wait;
			}
			break;

		case phase_dl_start_wait:
			if (abort) {
				if (gIpSetting == IP_SETTING_MANUAL) {
					arp_probe_cancel (if_num);
				}
				phase = phase_dl_stop;
				break;
			}
			if (gDeviceType == DEV_MODEM ||
				(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
				if (wait == 0) {
					ret = ppp_stat(&gPstat);
					if (gPstat.state == AT_PPP_STAT_FAIL) {
						phase = phase_dl_stop;
					} else if (gPstat.state == AT_PPP_STAT_ESTABLISHED) {
						gIfinfo.type = AT_IPv4;
						gIfinfo.v4address = gPstat.ipcp.local_ip;
						gIfinfo.v4netmask = AT_HTONL(0);
						gIfinfo.v4brc = AT_HTONL(0);
						gGateway = 0;
						gDns1.type = AT_IPv4;
						gDns1.v4addr = gPstat.ipcp.local_dns1;
						gDns2.type = AT_IPv4;
						gDns2.v4addr = gPstat.ipcp.local_dns2;
						phase = phase_if_config;
					}
					wait = 10;
				} else {
					wait--;
				}
				print_ppp_stat(gPstat.state);
			} else if (gIpSetting == IP_SETTING_DHCP) {
				if (wait == 0) {
					gDhcpStat = (AT_SINT16)DHCP_timer();
					if (gDhcpStat == DHCP_STATE_BOUND) {
						ret = (AT_SINT16)DHCP_get_gateway(AT_NULL, &gGateway);
						gDns1.type = AT_IPv4;
						gDns2.type = AT_IPv4;
						ret = (AT_SINT16)DHCP_get_dns(AT_NULL, AT_NULL, &gDns1.v4addr, &gDns2.v4addr);
						phase = phase_if_config;
					} else if (gDhcpStat == DHCP_STATE_TIMEOUT || gDhcpStat == DHCP_STATE_ERROR) {
						DHCP_terminate();
						phase = phase_dl_term;
					}
					wait = 6;
				} else {
					wait--;
				}
				print_dhcp_stat(gDhcpStat);
			} else if (gIpSetting == IP_SETTING_MANUAL) {
				if (gArpProbeResult != AT_API_PENDING) {
					switch (gArpProbeResult) {
					case 0:
						OSReport ("arp_probe() timeout.\n");
						phase = phase_if_config;
						break;
					case 1:
						OSReport ("The ARP response was received from %s about %s.\n",
								mac2str(gArpProbeMac), nint2ipdot(gIfinfo.v4address));
						phase = phase_dl_term;
						break;
					case AT_API_ERR_DEV:
						OSReport ("link down\n");
						phase = phase_dl_term;
						break;
					case AT_API_CANCELED:
					default:
						phase = phase_dl_term;
						break;
					}
				}
			}
			break;

		case phase_if_config:
			ret = if_config(if_num, &gIfinfo);
			if (ret < 0) {
				phase = phase_dl_stop;
				break;
			}
			if (gGateway) {
				ret = route4_add(0, 0, gGateway);
			}
			dns_clear_server();
			if (gDns1.v4addr)
				ret = dns_set_server(&gDns1);
			if (gDns2.v4addr)
				ret = dns_set_server(&gDns2);

			ret = if_get(if_num, &gIfinfo, sizeof(gIfinfo), gMacaddr);
			if (ret < 0) {
				phase = phase_dl_stop;
				break;
			}
			ret = route4_list(gRouteList, sizeof(gRouteList));
			if (ret < 0) {
				phase = phase_dl_stop;
			}
			gRouteNum = ret;
			phase = phase_test_select;
			break;

		case phase_test_select:
			if (abort) {
				phase = phase_if_down;
				break;
			}
			if ((test = selectTest()) < 0) {
				break;
			}
			phase = phase_test;
			break;

		case phase_test:
			switch (test) {
			case tt_udp_echo:
				ret = udp_echo_test(abort); /* UDP ECHO */
				break;
			case tt_http_get:
				ret = http_test(abort); /* HTTP GET */
				break;
			case tt_ping:
				ret = ping_test(abort);   /* PING */
				break;
			case tt_tcp_echo1:
				ret = tcp_echo_test1(abort); /* TCP ECHO 1*/
				break;
			case tt_tcp_echo2:
				ret = tcp_echo_test2(abort); /* TCP ECHO 2*/
				break;
			case tt_tcp_echo3:
				ret = tcp_echo_test3(abort); /* TCP ECHO 3*/
				break;
			case tt_tcp_chargen:
				ret = tcp_chargen_recv_test(abort); /* TCP CHARGEN */
				break;
			case tt_tcp_discard:
				ret = tcp_discard_send_test(abort); /* TCP DISCARD */
				break;
			case tt_tcp_discard_server:
				ret = tcp_discard_server_test(abort); /* TCP DISCARD SERVER*/
				break;
			case tt_dns_get_name:
				ret = dns_get_name_test(abort); /* DNS */
				break;
			case tt_show_ifinfo:
				ret = show_ifinfo(abort); /* I/F information display */
				break;
			case tt_show_route:
				ret = show_route(abort); /* Route information display */
				break;
			case tt_if_down:
				ret = 0;
				phase = phase_if_down;
				break;
			}

			if (abort) {
				phase = phase_if_down;
			} else if (ret) {
				phase = phase_test_select;
			}
			break;

		case phase_if_down:
			if (gGateway) {
				ret = route4_del(0, 0, gGateway);
			}
			gRouteNum = 0;
			ret = if_down(if_num, AT_IPany);
			phase = phase_dl_stop;
			break;

		case phase_dl_stop:
			if (gDeviceType == DEV_MODEM ||
				(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
				AT_PPP_STOP_ARG stoparg;

				stoparg.wait = 0;
				ret = ppp_stop(&stoparg);
				phase = phase_dl_stop_wait;
			} else if (gIpSetting == IP_SETTING_DHCP) {
				ret = (AT_SINT16)DHCP_release_nb(0, gIfinfo.v4address, 5);
				phase = phase_dl_stop_wait;
			} else {
				phase = phase_dl_term;
			}
			break;

		case phase_dl_stop_wait:
			if (gDeviceType == DEV_MODEM ||
				(gDeviceType == DEV_BBA && gIpSetting == IP_SETTING_PPPOE)) {
				if (wait == 0) {
					ret = ppp_stat(&gPstat);
					if (gPstat.state == AT_PPP_STAT_UNUSED) {
						phase = phase_dl_term;
					}
					wait = 60;
				} else {
					wait--;
				}
				print_ppp_stat(gPstat.state);
				print_ppp_error(gPstat.error);
			} else if (gIpSetting == IP_SETTING_DHCP) {
				if (wait == 0) {
					gDhcpStat = (AT_SINT16)DHCP_timer();
					if (gDhcpStat == DHCP_STATE_INIT || gDhcpStat == DHCP_STATE_ERROR) {
						DHCP_terminate();
						phase = phase_dl_term;
					}
					wait = 6;
				} else {
					wait--;
				}
				print_dhcp_stat(gDhcpStat);
				break;
			}
			break;

		case phase_dl_term:
			dns_term();
			if (gDeviceType == DEV_BBA) {
				ret = dix_term();
				ret = arp_term();
			}
			ret = ppp_term();

#ifdef AT_GC_DEBUG
			AT_PacketDumpStop();
			AT_PacketDumpStat(&dump_buf, &dump_data_bytes, &dump_packet_counts);
#endif /* AT_GC_DEBUG */

			phase = phase_restart_wait;
			break;

		case phase_restart_wait:
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> restart");
#ifdef AT_GC_DEBUG
			DEMOPrintf(x, y += 16, 0, "Packet capture");
			DEMOPrintf(x, y += 16, 0, "  dump buffer address 0x%08x", dump_buf);
			DEMOPrintf(x, y += 16, 0, "  capture");
			DEMOPrintf(x, y += 16, 0, "    %u bytes", dump_data_bytes);
			DEMOPrintf(x, y += 16, 0, "    %u packets", dump_packet_counts);
#endif /* AT_GC_DEBUG */
			if (gButton & PAD_BUTTON_A) {
				phase = phase_dl_init;
				abort = 0;
				break;
			}
			if (gButton & PAD_BUTTON_B) {
				phase = phase_term;
				break;
			}
			break;

		case phase_term:
			avetcp_term();
			OSReport("avetcp_term\n");
			OSFree(workarea);
			phase = phase_end;
			break;

		case phase_end:
			DEMOPrintf(x, y += 16, 0, "PROGRAM END");
			DEMOPrintf(x, y += 16, 0, "PUSH A Button ==> restart");
			if (gButton & PAD_BUTTON_A) {
				phase = phase_init;
				abort = 0;
			}
			break;

		case phase_assert:
			DEMOPrintf(x, y += 16, 0, "ASSERT");
			break;
	}
}

static void set_pmprop(AT_PPP_MODEM_PROPERTY* pmprop)
{

	pmprop->needToInit			= 1;
	pmprop->needToDial			= 1;
	pmprop->needToCd			= 1;
	pmprop->needToDisc			= 0;
	pmprop->countryCode			= AT_NULL;
	pmprop->connectMode			= AT_GC_MDM_CN_DEFAULT;
	pmprop->errorCorrectMode	= AT_GC_MDM_EC_DEFAULT;
	pmprop->compressMode		= AT_GC_MDM_CM_DEFAULT;
	pmprop->atcommand_count		= 0;
	pmprop->atcommand			= AT_NULL;
}

static void set_psarg(AT_PPP_START_ARG* psarg, s32 pppoe)
{

	psarg->mode					= PP_MODE_DIALOUT;
	if (pppoe)
		psarg->mode				|= PP_MODE_PPPOE;
	psarg->iptype				= AT_IPv4;
	psarg->wait					= 0;
	psarg->login				= (unsigned char *)LOGIN_NAME;
	psarg->password				= (unsigned char *)LOGIN_PASSWD;
	psarg->tele_number			= (unsigned char *)TELEPHONE_NUM;
	psarg->notify				= ppp_connect_notify;
	psarg->tele.dialtype		= 0;			/* Tone:0, Pulse:1 */
	psarg->tele.outside_line	= 0;
	psarg->tele.outside_number	= AT_NULL;
	psarg->tele.timeout			= 180;
	psarg->conn.recognize		= 4;			/* Set authentication (depending on other party)*/
	psarg->conn.mru				= 1500;
	psarg->conn.acfcomp			= 0;
	psarg->conn.protocomp		= 0;
	psarg->conn.vjcomp			= 0;
	psarg->ipcp.local_ip		= 0;
	psarg->ipcp.remote_ip		= 0;
	psarg->ipcp.local_dns1		= 0;
	psarg->ipcp.local_dns2		= 0;
	psarg->ipcp.remote_dns1		= 0;
	psarg->ipcp.remote_dns2		= 0;
}

static s16 selectIpSetting(void)
{
	static s16 sel = 0;

	if (gButton & PAD_BUTTON_DOWN) {
		sel++;
		if (sel > 2) sel = 0;
	}
	if (gButton & PAD_BUTTON_UP) {
		sel--;
		if (sel < 0) sel = 2;
	}
	DEMOPrintf(x, y += 16, 0, "%c IP Manual Setting (IP: %s)", (sel == 0) ? '*' : ' ', nint2ipdot(AT_HTONL(MY_IPADDR)));
	DEMOPrintf(x, y += 16, 0, "%c DHCP", (sel == 1) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c PPPoE (ID: %s)", (sel == 2) ? '*' : ' ', LOGIN_NAME);
				
	if (gButton & PAD_BUTTON_A) {
		return (sel);
	} else {
		return (-1);
	}
}

static s16 selectTest(void)
{
	static s16 sel = 0;
	int test_num;

	test_num = (int)tt_test_num;

	if (gButton & PAD_BUTTON_DOWN) {
		sel++;
		if (sel >= test_num) sel = 0;
	}
	if (gButton & PAD_BUTTON_UP) {
		sel--;
		if (sel < 0) sel = (s16)(test_num - 1);
	}
	DEMOPrintf(x, y += 16, 0, "%c interface down", (sel == (s16)tt_if_down) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c show interface info", (sel == (s16)tt_show_ifinfo) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c show route", (sel == (s16)tt_show_route) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c get name", (sel == (s16)tt_dns_get_name) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c udp echo", (sel == (s16)tt_udp_echo) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c http get", (sel == (s16)tt_http_get) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp echo (1)", (sel == (s16)tt_tcp_echo1) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp echo (2)", (sel == (s16)tt_tcp_echo2) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp echo (3)", (sel == (s16)tt_tcp_echo3) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp chargen", (sel == (s16)tt_tcp_chargen) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp discard", (sel == (s16)tt_tcp_discard) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c tcp discard server", (sel == (s16)tt_tcp_discard_server) ? '*' : ' ');
	DEMOPrintf(x, y += 16, 0, "%c ping", (sel == (s16)tt_ping) ? '*' : ' ');
	
	if (gButton & PAD_BUTTON_A) {
		return (sel);
	} else {
		return (-1);
	}
}

static AT_VOID ppp_connect_notify(AT_SINT32 result, AT_SINT32 speed)
{
	OSReport(" ppp_connect_notify: result %d speed %d\n", result, speed);
}

static AT_VOID arp_probe_notify (AT_SINT16 result, AT_UBYTE* mac)
{
	OSReport (" arp_probe_notify: result %d\n", result);
	gArpProbeResult = result;
	if (result == 1) {
		memcpy (gArpProbeMac, mac, 6);
	}
}

char* nint2ipdot(AT_NINT32 data)
{
	static char buf[16];
	unsigned char* p;

	p = (unsigned char*)&data;
	sprintf(buf, "%d.%d.%d.%d", *p, *(p+1), *(p+2), *(p+3));

	return buf;
}

char* mac2str(AT_UBYTE* mac)
{
	static char buf[18];

	sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X",
			*mac, *(mac+1), *(mac+2), *(mac+3), *(mac+4), *(mac+5));

	return buf;
}

void print_dhcp_stat(AT_SINT16 stat)
{

	switch (stat) {
	case DHCP_STATE_INIT:
		DEMOPrintf(x, y += 16, 0, "DHCP status: INIT");
		break;
	case DHCP_STATE_SELECTING:
		DEMOPrintf(x, y += 16, 0, "DHCP status: SELECTING");
		break;
	case DHCP_STATE_REQUESTING:
		DEMOPrintf(x, y += 16, 0, "DHCP status: REQUESTING");
		break;
	case DHCP_STATE_BOUND:
		DEMOPrintf(x, y += 16, 0, "DHCP status: BOUND");
		break;
	case DHCP_STATE_RENEWING:
		DEMOPrintf(x, y += 16, 0, "DHCP status: RENEWING");
		break;
	case DHCP_STATE_REBINDING:
		DEMOPrintf(x, y += 16, 0, "DHCP status: REBINDING");
		break;
	case DHCP_STATE_RELEASING:
		DEMOPrintf(x, y += 16, 0, "DHCP status: RELEASING");
		break;
	case DHCP_STATE_TIMEOUT:
		DEMOPrintf(x, y += 16, 0, "DHCP status: TIMEOUT");
		break;
	case DHCP_STATE_ERROR:
		DEMOPrintf(x, y += 16, 0, "DHCP status: ERROR");
		break;
	}
}
	
void print_ppp_stat(AT_SINT16 stat)
{

	switch (stat) {
	case AT_PPP_STAT_UNUSED:
		DEMOPrintf(x, y += 16, 0, "PPP status: UNUSED");
		break;
	case AT_PPP_STAT_OPEN_STANDBY:
		DEMOPrintf(x, y += 16, 0, "PPP status: OPEN STANDBY");
		break;
	case AT_PPP_STAT_DIALING:
		DEMOPrintf(x, y += 16, 0, "PPP status: DIALING");
		break;
	case AT_PPP_STAT_AUTH:
		DEMOPrintf(x, y += 16, 0, "PPP status: AUTH");
		break;
	case AT_PPP_STAT_ESTABLISHED:
		DEMOPrintf(x, y += 16, 0, "PPP status: ESTABLISHED");
		break;
	case AT_PPP_STAT_DISCONNECTED:
		DEMOPrintf(x, y += 16, 0, "PPP status: DISCONNECTED");
		break;
	case AT_PPP_STAT_CLOSE_STANDBY:
		DEMOPrintf(x, y += 16, 0, "PPP status: CLOSE STANDBY");
		break;
	case AT_PPP_STAT_HANG_UP:
		DEMOPrintf(x, y += 16, 0, "PPP status: HANG UP");
		break;
	case AT_PPP_STAT_FAIL:
		DEMOPrintf(x, y += 16, 0, "PPP status: FAIL");
		break;
	}
}

void print_ppp_error(AT_SINT16 err)
{

	switch (err) {
	case AT_PPP_API_SUCCESS:
		DEMOPrintf(x, y += 16, 0, "PPP no error");
		break;
	case AT_PPP_API_ERR_MODEM:
		DEMOPrintf(x, y += 16, 0, "PPP error: MODEM");
		break;
	case AT_PPP_API_ERR_BUSY:
		DEMOPrintf(x, y += 16, 0, "PPP error: BUSY");
		break;
	case AT_PPP_API_ERR_NO_DIAL_TONE:
		DEMOPrintf(x, y += 16, 0, "PPP error: NO DIALTONE");
		break;
	case AT_PPP_API_ERR_SCRIPT:
		DEMOPrintf(x, y += 16, 0, "PPP error: SCRIPT");
		break;
	case AT_PPP_API_ERR_LCP:
		DEMOPrintf(x, y += 16, 0, "PPP error: LCP");
		break;
	case AT_PPP_API_ERR_AUTH:
		DEMOPrintf(x, y += 16, 0, "PPP error: AUTH");
		break;
	case AT_PPP_API_ERR_IPCP:
		DEMOPrintf(x, y += 16, 0, "PPP error: IPCP");
		break;
	case AT_PPP_API_ERR_NO_CARRIER:
		DEMOPrintf(x, y += 16, 0, "PPP error: NO CARRIER");
		break;
	case AT_PPP_API_ERR_NO_ANSWER:
		DEMOPrintf(x, y += 16, 0, "PPP error: NO ANSWER");
		break;
	case AT_PPP_API_ERR_DELAYED:
		DEMOPrintf(x, y += 16, 0, "PPP error: DELAYED");
		break;
	case AT_PPP_API_ERR_BLACKLISTED:
		DEMOPrintf(x, y += 16, 0, "PPP error: BLACKLISTED");
		break;
	case AT_PPP_API_ERR_TIME_OUT:
		DEMOPrintf(x, y += 16, 0, "PPP error: TIMEOUT");
		break;
	case AT_PPP_API_ERR_USER_REQ:
		DEMOPrintf(x, y += 16, 0, "PPP error: USER REQ");
		break;
	}
}

char* tcpstat2str(AT_SINT16 stat)
{
	static char out_str[13];

	switch (stat) {
	case AT_STAT_CLOSED:
		sprintf(out_str, "CLOSED");
		break;
	case AT_STAT_LISTEN:
		sprintf(out_str, "LISTEN");
		break;
	case AT_STAT_SYN_SENT:
		sprintf(out_str, "SYN_SENT");
		break;
	case AT_STAT_SYN_RECVD:
		sprintf(out_str, "SYN_RECVD");
		break;
	case AT_STAT_ESTABLISHED:
		sprintf(out_str, "ESTABLISHED");
		break;
	case AT_STAT_CLOSE_WAIT:
		sprintf(out_str, "CLOSE_WAIT");
		break;
	case AT_STAT_FIN_WAIT_1:
		sprintf(out_str, "FIN_WAIT_1");
		break;
	case AT_STAT_CLOSING:
		sprintf(out_str, "CLOSING");
		break;
	case AT_STAT_LAST_ACK:
		sprintf(out_str, "LAST_ACK");
		break;
	case AT_STAT_FIN_WAIT_2:
		sprintf(out_str, "FIN_WAIT_2");
		break;
	case AT_STAT_TIME_WAIT:
		sprintf(out_str, "TIME_WAIT");
		break;
	case AT_STAT_FATAL:
		sprintf(out_str, "FATAL");
		break;
	}

	return out_str;
}

char* tm2timestr(wave_tm *ptm)
{
	static char out_str[25];
	static char *month_str[12] = {
		"Jan","Feb","Mar","Apr","May","Jun",
		"Jul","Aug","Sep","Oct","Nov","Dec"
	};
	static char *week_str[7] = {
		"Sun","Mon","Tue","Wed","Thu","Fri","Sat" 
	};

	sprintf(out_str, "%s %s %02d %02d:%02d:%02d %04d",
			week_str[ptm->tm_wday], month_str[ptm->tm_mon], ptm->tm_mday,
			ptm->tm_hour, ptm->tm_min, ptm->tm_sec, ptm->tm_year + 2000
			);

	return out_str;
}

wave_tm* wave_gmtime(u32 t)
{
	int day;
	int y4day;
	int m_days;
	int i,h;
	static wave_tm tm;
	static int month_days[12] = {
		31,28,31,30,31,30,31,31,30,31,30,31
	};
	static int uru[144] = {
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2000 - 2015 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2016 - 2031 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2032 - 2047 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2048 - 2063 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2064 - 2079 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2080 - 2095 */
		1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0, /* 2096 - 2111 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0, /* 2112 - 2127 */
		1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0  /* 2128 - 2143 */
	};

	tm.tm_sec = (int)(t % 60);
	tm.tm_min = (int)((t / 60) % 60);
	tm.tm_hour = (int)((t / 60 / 60) % 24);
	
	day = (int)(t / 60 / 60 / 24);
	tm.tm_wday = (day + 6) % 7;

	for (i = 0, h = 0, tm.tm_yday = day; i < 144; i++) {
		y4day = uru[i] ? 366 : 365;
		h += y4day;
		if (h > day) {
			tm.tm_year = i;
			break;
		}
		tm.tm_yday -= y4day;
	}

	for (i = 0, h = tm.tm_yday; ; i++) {
		if ((i == 1) && (uru[tm.tm_year])) {
			m_days = 29;
		} else {
			m_days = month_days[i];
		}
		if (h < m_days) {
			break;
		} else {
			h -= m_days;
		}
	}
	tm.tm_mon = i;
	tm.tm_mday = h + 1;

	return &tm;
}
	
