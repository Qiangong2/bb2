
#include <bbtypes.h>
#include <bbsec.h>
#include <bsl.h>
#include <aes_api.h>

#include <t2os.h>
#include <t2stdio.h>
#include "TDisplayEngine.h"
#include "dsplib.h"

#include "t2typedef.h"
#include "t2mpgdec.h"
#include "TDisplayEngine.h"
#include "dsplib.h"
#include "t2stdio.h"
#include "t2os.h"
#include "BIOS_vkeys.h"


#include "pci_init.h"
#include "ip_eth.h"


#include "common_utils.c"

#define DATA_BLK_SIZE	16384
#define AES_BLOCK_SIZE  128
#define AES_BLOCK_SIZE_BYTES 16
#define HASH_SIZE_BYTES 20
#define CHUNK_SIZE 1024
	
int testmain(){
    
    	u8 aesKey[AES_BLOCK_SIZE_BYTES];
    	u8 aesIv[AES_BLOCK_SIZE_BYTES];
    	u8 plainTextBytes[DATA_BLK_SIZE];
    	u8 cipherTextBytes[DATA_BLK_SIZE];
    	u8 hash[HASH_SIZE_BYTES];
    	u8 tempblock[CHUNK_SIZE];
    	AesKeyInstance key;
    	AesCipherInstance cipher;
    	BbShaContext sha;
    	int error;
	u32 start_time, elapsed_time;
    
    	unsigned int seed 	= 0x12345678;
    	int i;	

    	/* get random data 
     	*/
    	for(i = 0; i < DATA_BLK_SIZE; i++){
      		plainTextBytes[i] = myrand(seed) & 0x000000ff;
     	 	seed = plainTextBytes[i];
    	}	
    
	soc_printf("got random data \n");
    	/* get random key and IV 
     	*/
    	for(i=0; i < AES_BLOCK_SIZE_BYTES; i++){
      		aesKey[i] = myrand(seed) & 0x000000ff;
      		seed = aesKey[i];
      		aesIv[i] = myrand(seed) & 0x000000ff;
      		seed = aesIv[i];
    	}
	soc_printf("got random key and IV\n");
    
    	/* initialize
     	*/
 
    	start_time = get_count();
	soc_printf("got elapsed_time = %ld\n", get_count() - start_time);
	start_time = get_count();
    	if((error = aesMakeKey(&key, 
				AES_DIR_ENCRYPT, 
				AES_BLOCK_SIZE, 
				aesKey)) != AES_TRUE){
		soc_printf("Error creating aes key.\n");
      		goto cleanup;
    	}
    
    	if ((error = aesCipherInit (&cipher, 
				AES_MODE_CBC, 
				aesIv)) != AES_TRUE) {
        	soc_printf("Error initializing cipher.\n");
        	goto cleanup;
    	}
	
    	/* iterate over blocks, encrypting 
     	*/
    	for(i=0; i< DATA_BLK_SIZE; i += CHUNK_SIZE){
      		error = aesBlockEncrypt(&cipher, 
			      &key, 
			      plainTextBytes + i,
			      CHUNK_SIZE*8, 
			      cipherTextBytes + i);
      		if (error <= 0) {
			soc_printf("Failed to encrypt block.\n");
			goto cleanup;
      		}
    	}
	soc_printf("got time for encrypt = %ld\n", get_count() - start_time);

	       
    	/* decrypt to verify and time
     	*/
    	start_time = get_count();

    	error = aesMakeKey(&key, AES_DIR_DECRYPT, AES_BLOCK_SIZE, aesKey);
    	if (error != AES_TRUE) {
      		soc_printf("Error creating aes key.\n");
      		goto cleanup;
    	}
	
    
    	if ((error = aesCipherInit (&cipher, 
				AES_MODE_CBC, 
				aesIv)) != AES_TRUE) {
        	soc_printf("Error initializing cipher.\n");
        	goto cleanup;
    	}
    
    	/* iterate over blocks, decrypting 
     	*/
    	for(i=0; i< DATA_BLK_SIZE; i += CHUNK_SIZE){
      		error = aesBlockDecrypt(&cipher, 
				      &key, 
				      cipherTextBytes + i,
				      CHUNK_SIZE*8, 
				      tempblock);
      		if (error <= 0) {
			soc_printf("Failed to decrypt block.\n");
			goto cleanup;
      		}
#if 0
      		if((memcmp(tempblock, plainTextBytes + i, CHUNK_SIZE)) !=0){
			soc_printf("Failed to compare after decryption\n");
			goto cleanup;
      		}
#endif
	}
      	
    	elapsed_time = get_count() - start_time;
	soc_printf("time for decrypt = %ld\n", elapsed_time);
       	soc_printf("decryption compare SUCCEEDED!\n");
    	/* do SHA calculation to time
     	*/
    	start_time = get_count();
    	bsl_sha_reset(&sha);
    	for(i=0; i < DATA_BLK_SIZE; i += CHUNK_SIZE){
      		bsl_sha_input(&sha, plainTextBytes + i, CHUNK_SIZE);
    	}
    	bsl_sha_result(&sha, hash);
	elapsed_time = get_count() - start_time;
	soc_printf("time for sha = %ld\n", elapsed_time);

	/* time a small block */
	start_time = get_count();
	bsl_sha_reset(&sha);
	bsl_sha_input(&sha, "aaa", sizeof("aaa"));
	bsl_sha_result(&sha, hash);
	elapsed_time = get_count() - start_time;
	soc_printf("time for small block sha = %ld\n", elapsed_time);

	/* time the three components separately */
	for(;;){
	start_time = get_count();
	bsl_sha_reset(&sha);
	elapsed_time = get_count() - start_time;
	soc_printf("time for small block reset = %ld\n", elapsed_time);

	start_time = get_count();
	bsl_sha_input(&sha, "aaaaaaaaaaaaaaaaaaaaa", sizeof("aaa"));
	elapsed_time = get_count() - start_time;
	soc_printf("time for small block sha input = %ld\n", elapsed_time);


	start_time = get_count();
	bsl_sha_result(&sha, hash);
	elapsed_time = get_count() - start_time;
	soc_printf("time for small block sha result = %ld\n", elapsed_time);
	}
	/* spin on while forever
	*/
	while(1);
       
    	return 0;
cleanup: 
	soc_printf("ERROR!\n");
    	return -1;

}


void t2os_main(void){
	ER ercd;
	T_CTSK t_ctsk;
	ER ret;
	
	ret = t2os_kernel_init(&osp);
	if(ret != E_OK){
		soc_printf("OS Init failed!\n");
		while(1);
	}
	
	//Create one task
	soc_printf("Create one task!\n");
	t_ctsk.task = (FP) testmain;
	t_ctsk.stksz = 0x10000; //64 K stack size
	t_ctsk.quantum = 10;
	t_ctsk.itskpri = 20;
	ercd = cre_tsk(TASK_ID_MAIN, &t_ctsk);

	//Start one task 
	if((ercd = sta_tsk(TASK_ID_MAIN, 0)) != E_OK){
		soc_printf("Starting one task failed!\n");
	}
	t2os_kernel_start();


}
