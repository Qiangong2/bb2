
#include <bbtypes.h>
#include <bbsec.h>
#include <bsl.h>
#include <aes_api.h>
#include <poly_math.h>


#include <t2os.h>
#include <t2stdio.h>
#include "TDisplayEngine.h"
#include "dsplib.h"

#include "t2typedef.h"
#include "t2mpgdec.h"
#include "TDisplayEngine.h"
#include "dsplib.h"
#include "t2stdio.h"
#include "t2os.h"
#include "BIOS_vkeys.h"


#include "pci_init.h"
#include "ip_eth.h"

#include "common_utils.c"


int testmain(){
    
    BbEccPrivateKey private_key1;
    BbEccPublicKey public_key1;
    BbEccPrivateKey private_key2;
    BbEccPublicKey public_key2;
    BbAesKey shared_key1;
    BbAesKey shared_key2;
    u8 input_string[1024];
    u32 rand_input[8];
    u32 start_time, elapsed_time;
    BbEccSig eccsign;
    BSL_error res;
    field_2n a,b,c;
    int k;
    
    unsigned int seed = 0x12345678;
    
    int i, same;
    int iter =0;
    u32 rand_bit, rand_word, mask;


    for(iter = 0; iter < 50; iter++){
    /* simple poly math tests */
      for(i=0; i< 8; i++){
	a.e[i] = myrand(seed) & 0x000000ff;
	seed = a.e[i];
	b.e[i] = myrand(seed) & 0x000000ff;
	seed = b.e[i];
      }
      a.e[0] &= UPR_MASK;
      b.e[0] &= UPR_MASK;
      poly_mul(&a, &b, &c);
    
      poly_inv(&a, &b);
      poly_mul(&a, &b, &c);

      same = 0;
      for(i=0; i< 7; i++){
	if (c.e[i] != 0){
	  same = 1;
	}
      }
      if(c.e[7] != 0x1){
	same = 1;
      }
    
      /* c should be one */
      if(same == 0)
	soc_printf("POLY TEST PASS %d \n", iter);
      else
	soc_printf("POLY TEST FAIL %d \n", iter);
    }



    /* public key and shared key tests */
   

    for(iter = 0; iter < 1; iter++){
    /* random private key */
      for(i=0; i< 8; i++){
	private_key1[i] = myrand(seed);
	seed = private_key1[i];
      }
      private_key1[0] &= UPR_MASK;
      
      private_key1[0] = 0x00000179;
      private_key1[1] = 0xcda7e2fe;
      private_key1[2] = 0x16d77e37;
      private_key1[3] = 0x39fbb3de;
      private_key1[4] = 0x618d5d57;
      private_key1[5] = 0x9064949b;
      private_key1[6] = 0x7721c36c;
      private_key1[7] = 0x0a66a2a6;
      
      /* for timing : iterate */
      
      start_time = get_count();
      for(k = 0; k < 5; k++)
	bsl_ecc_gen_public_key(public_key1, private_key1);

      elapsed_time = get_count() - start_time;
      soc_printf("time for key gen = %d \n", elapsed_time/100/5);
                
      
      /* random private key */
      for(i=0; i< 8; i++){
	private_key2[i] = myrand(seed);
	seed = private_key2[i];
      }
      private_key2[0] &= UPR_MASK;

      private_key2[0] = 0x0000017a;
      private_key2[1] = 0xcda7e2fe;
      private_key2[2] = 0x16d77e37;
      private_key2[3] = 0x39fbb3de;
      private_key2[4] = 0x618d5d57;
      private_key2[5] = 0x9064949b;
      private_key2[6] = 0x7721c36c;
      private_key2[7] = 0x0a66a2a6;
      
      

      bsl_ecc_gen_public_key(public_key2, private_key2);

      /* generate the two shared keys */

      /* iterate to time */
      
      start_time = get_count();
      for(k = 0; k < 5; k++)
	bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      elapsed_time = get_count() - start_time;


      soc_printf("time for shared key = %d \n", elapsed_time/100/5);
        
      bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      bsl_ecc_gen_shared_key(public_key2, private_key1, shared_key2);
      same = 0;
      for(i=0; i < sizeof(BbAesKey)/sizeof(u32); i++){
	soc_printf("shared key 1 = %08x 2 = %08x\n", (unsigned int)shared_key1[i], (unsigned int)shared_key2[i]);
	if(shared_key1[i] != shared_key2[i]){
	  same = 1;
	}
      }
      if(same == 0)
	soc_printf("KEY GEN PASS %d \n", iter);
      else
	soc_printf("KEY GEN FAIL %d \n", iter);


      /* negative test, purturb private_key 2 */
      rand_word = ((myrand(seed) & 0xff) % 7) + 1;
      rand_bit = myrand(seed) & 0xff;
      seed = rand_bit;
      mask = 0x1 << (rand_bit % 31);
      soc_printf("mask = %08x\n", (unsigned int )mask);
      soc_printf("rand_word= %08x\n", (unsigned int )rand_word);
      private_key2[rand_word] = private_key2[rand_word] ^ mask;
      bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      same = 0;
      for(i=0; i < sizeof(BbAesKey)/sizeof(u32); i++){
	soc_printf("shared key 1 = %08x 2 = %08x\n", (unsigned int)shared_key1[i], (unsigned int)shared_key2[i]);
	if(shared_key1[i] != shared_key2[i]){
	  same = 1;
	}
      }
      if(same == 1)
	soc_printf("KEY GEN NEGATIVE TEST PASS %d \n", iter);
      else
	soc_printf("KEY GEN NEGATIVE TEST FAIL %d \n", iter);

      
    }
  
    /* one test for zeros in signature, small input */

    for(i = 0; i < 1024; i++){
      input_string[i] = 0x0;
    }
   
    for(i=0; i< 8; i++){
      private_key1[i] = myrand(seed);
      seed = private_key1[i];
    }
    private_key1[0] &= UPR_MASK;
    
    for(i=0; i< 8; i++){
      rand_input[i] = myrand(seed);
      seed = rand_input[i];
    }
    rand_input[0] &= UPR_MASK;
    
    bsl_ecc_compute_sig(input_string, 4, private_key1, rand_input, eccsign, 0x30);
      
    for(i =0; i< 16; i++){
      eccsign[i] = 0x0;
    }
      
    bsl_ecc_gen_public_key(public_key1, private_key1);
    res = bsl_ecc_verify_sig(input_string, 4, public_key1, eccsign, 0x00);
    
      
    if(res == BSL_OK){
      soc_printf("SIGN VERIFY FAIL ZEROS TEST \n");
    }
    else{
      soc_printf("SIGN VERIFY PASS ZEROS TEST \n");
    }


    /* test the ECDSA part iteratively */
    for(iter = 0; iter < 1; iter++){
      for(i = 0; i < 1024; i++){
        input_string[i] = myrand(seed) & 0xff;
	seed = input_string[i];
      }
    
      for(i=0; i< 8; i++){
	private_key1[i] = myrand(seed);
	seed = private_key1[i];
      }
      private_key1[0] &= UPR_MASK;
         
      for(i=0; i< 8; i++){
	rand_input[i] = myrand(seed);
	seed = rand_input[i];
      }
      rand_input[0] &= UPR_MASK;
         
      private_key1[0] = 0x00000179;
      private_key1[1] = 0xcda7e2fe;
      private_key1[2] = 0x16d77e37;
      private_key1[3] = 0x39fbb3de;
      private_key1[4] = 0x618d5d57;
      private_key1[5] = 0x9064949b;
      private_key1[6] = 0x7721c36c;
      private_key1[7] = 0x0a66a2a6;
      
       
      rand_input[0] = (0x00000019);
      rand_input[1] = (0xcda7e2fe);
      rand_input[2] = (0x16d77e37);
      rand_input[3] = (0x39fbb3de);
      rand_input[4] = (0x618d5d57);
      rand_input[5] = (0x9064949b);
      rand_input[6] = (0x7721c36c);
      rand_input[7] = (0x0a66a2a6);
      
      /* iterate to time */
      
      start_time = get_count();
      for(k = 0; k < 50; k++)
	bsl_ecc_compute_sig(input_string, 1024, private_key1, rand_input, eccsign, 0x30);
      elapsed_time = get_count() - start_time;

      soc_printf("time for sign(microsec) = %d \n", elapsed_time/100/50);
        
      bsl_ecc_compute_sig(input_string, 1024, private_key1, rand_input, eccsign, 0x30);

      
      bsl_ecc_gen_public_key(public_key1, private_key1);


      start_time = get_count();
      for(k = 0; k < 50; k++)
	res = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign, 0x30);
      elapsed_time = get_count() - start_time;

      soc_printf("time for verify(microsec)= %d \n", elapsed_time/100/50);
        
      res = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign, 0x30);
    
      
      if(res == BSL_OK){
        soc_printf("SIGN VERIFY PASS %d\n", iter);
      }
      else{
        soc_printf("SIGN VERIFY FAIL %d\n", iter);
      }
      /* change a bit */
      rand_word = myrand(seed) & 0x7;
      rand_bit = myrand(seed) & 0x7f;
      mask = 0x1 << (rand_bit % 32);
      eccsign[rand_word] = eccsign[rand_word] ^ mask;
      
      bsl_ecc_gen_public_key(public_key1, private_key1);
      res = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign, 0x30);
    
      soc_printf("rand_word = %ld\n", rand_word);
      soc_printf("rand_bit = %ld\n", rand_bit);
      soc_printf("mask = %08x\n", (unsigned int )mask);

      
      if(res != BSL_OK){
        soc_printf("SIGN VERIFY NEG TEST PASS FINALLY %d\n", iter);
      }
      else{
        soc_printf("SIGN VERIFY NEG TEST FAIL FINALLY %d\n", iter);
      }
      
    }
        
    /* spin on while forever
     */
	while(1);
    return 0;
}


void t2os_main(void){
	ER ercd;
	T_CTSK t_ctsk;
	ER ret;
	
	ret = t2os_kernel_init(&osp);
	if(ret != E_OK){
		soc_printf("OS Init failed!\n");
		while(1);
	}
	
	//Create one task
	soc_printf("Create one task!\n");
	t_ctsk.task = (FP) testmain;
	t_ctsk.stksz = 0x10000; //64 K stack size
	t_ctsk.quantum = 10;
	t_ctsk.itskpri = 20;
	ercd = cre_tsk(TASK_ID_MAIN, &t_ctsk);

	//Start one task 
	if((ercd = sta_tsk(TASK_ID_MAIN, 0)) != E_OK){
		soc_printf("Starting one task failed!\n");
	}
	t2os_kernel_start();

}
