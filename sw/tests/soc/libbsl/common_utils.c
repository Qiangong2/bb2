
/* add more ids if there are more tasks 
*/
#define TASK_ID_MAIN 1
/* ##### OS BOOTUP PARAMETERS ############################### */

#define CPU_FREQUENCY 200000000


/* IRQ stack size */
#define SIZE_IRQSTACK	0x8000
/* SYS stack size, shares by tasks (also cyclic and alarm) */
#define SIZE_SYSSTACK	0x20000
/* 
SYS data structure size, share by semaphores, 
flags, message buffer controller 
*/
#define SIZE_DATA		0x4000
/* total size 
*/
#define SIZE_OSMEMORY (SIZE_IRQSTACK + SIZE_SYSSTACK + SIZE_DATA)


/* os memory block, deservedly put in data segment, not bss */
static char os_memory[SIZE_OSMEMORY] = {0};
/* operation system parameters */
static oslib_parameter_t osp = 
{
	CPU_FREQUENCY / 2 / 1000, 	/* timer_tick default to 
					1ms (MHz / 2 / 1000 = 1ms) */
	(unsigned long)os_memory, 	/* pointer to os reserved memory */
	SIZE_OSMEMORY,			/* os reserved memory size, 
					it must be bigger than the sum of 
					stacks plus 16K for other 
					DS(flags, cyclics, tasks...) */
	SIZE_IRQSTACK, 			/* stack size reserved by 
					interrupt service routines */
	SIZE_SYSSTACK,			/* system stack size, the system 
					stack must be bigger than the sum of 
					all task's stack */
	20, 				/* the number of tasks, so the taskid could be 1 to 20, */
	20, 				/* cyclic number (5-24)*/
	20, 				/* alarm number  (5-24)*/
	20, 				/* message buffer number (5-24)*/
	40, 				/* semaphore number (5-24)*/
	20, 				/* define flag number (5-24)*/
	0       			/* PLEASE SET THIS ITEM TO 
					ZERO IF YOU ARE NOT THE OS DEVELOPER */
};


/* general utilities 
*/

unsigned int myrand(unsigned int seed){
	seed = 0x343fd*seed + 0x269ec3;
	return ((seed) & (0xffffffff));
}

static u32 get_count(void) {
    u32 count;

    __asm__ volatile (
        "mfc0\t%0,$9\n\t"
        : "=r" (count));
 
    return count;
}

#if 0
int memcmp(const void *s1, const void *s2, size_t n){
    	const char* a = s1, * b = s2;
    	char a1, b1;
    	while (n-- > 0) {
		if ((a1 = *a++) == (b1 = *b++)) continue;
		return a1 - b1;
    	}
   	return 0;
}
#endif

char m_str[512];

extern void soc_printf(char *fmt,...);

