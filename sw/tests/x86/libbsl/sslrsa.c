#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <stdlib.h>
#include <bbsec.h>
#include <rsautil.h>
  
void run_tests(int iter, int num_bits){
  int j, i, num_bytes, size;
  RSA *prsa;
  int ret;
  for(j=0; j< iter; j++){
    prsa = RSA_new();
    num_bytes = num_bits/8;
    prsa = RSA_generate_key(num_bits, 3, NULL, NULL);
    size = RSA_size(prsa);
    printf("New %d bit Key\n", num_bits);
    for(i=0; i< 10; i++){
      ret = validate(prsa, size*8);
      if(ret ==0){
            printf("OK PASS test %d, iter = %d checking RSA verify\n", i,j);
        }
        else {
            printf("NOT OK FAIL test %d, iter = %d checking RSA verify\n",i, j);
        }
    }
    RSA_free(prsa);
  }
}



int main(int argc, char **argv){
  /* run_tests(iterations, bit length ) */
    run_tests(10, 2048);
    run_tests(10, 4096);

    return 0;
}
