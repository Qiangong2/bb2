
#include <stdio.h>

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <time.h>
#include <bbtypes.h>
#include <bbsec.h>
#include <bsl.h>
#include <aes_api.h>
#include <aes.h>

#include <stdlib.h>


#include <bsl.h>

#define DATA_BLK_SIZE	16384
#define AES_BLOCK_SIZE  128
#define AES_BLOCK_SIZE_BYTES 16
#define HASH_SIZE_BYTES 20
#define CHUNK_SIZE 1024


int main(){
    
    u8 aesKey[AES_BLOCK_SIZE_BYTES];
    u8 aesIv[AES_BLOCK_SIZE_BYTES];
    u8 plainTextBytes[DATA_BLK_SIZE];
    u8 cipherTextBytes[DATA_BLK_SIZE];
    u8 hash[HASH_SIZE_BYTES];
    u8 tempblock[CHUNK_SIZE];
    u8 hmacKey[20];

    AesKeyInstance key;
    AesCipherInstance cipher;
    BbShaContext sha;
    int error;
    
    struct timeb tp1, tp2;
    unsigned int seed;
    int i;

    seed = getpid();
    srand(seed);

    /* get random data 
     */
    for(i = 0; i < DATA_BLK_SIZE; i++){
      plainTextBytes[i] = rand() & 0x000000ff;
    }
    
    /* get random key and IV 
     */
    for(i=0; i < AES_BLOCK_SIZE_BYTES; i++){
      aesKey[i] = rand() & 0x000000ff;
      aesIv[i] = rand() & 0x000000ff;
    }
    
    /* initialize
     */
 
    (void) ftime(&tp1);
    if((error = aesMakeKey(&key, AES_DIR_ENCRYPT, AES_BLOCK_SIZE, aesKey)) != AES_TRUE){
      fprintf(stderr,"Error creating aes key.\n");
      goto cleanup;
    }
    
    if ((error = aesCipherInit (&cipher, AES_MODE_CBC, aesIv)) != AES_TRUE) {
        fprintf(stderr,"Error initializing cipher.\n");
        goto cleanup;
    }

    /* iterate over blocks, encrypting 
     */
    for(i=0; i< DATA_BLK_SIZE; i += CHUNK_SIZE){
      error = aesBlockEncrypt(&cipher, 
			      &key, 
			      plainTextBytes + i,
			      CHUNK_SIZE*8, 
			      cipherTextBytes + i);
      if (error <= 0) {
	fprintf(stderr,"Failed to encrypt block.\n");
	goto cleanup;
      }
    }

    (void) ftime(&tp2);
    printf("time to encrypt %d bytes = %d \n", DATA_BLK_SIZE,(tp2.time - tp1.time));
       
    /* decrypt to verify and time
     */
    (void) ftime(&tp1);
    error = aesMakeKey(&key, AES_DIR_DECRYPT, AES_BLOCK_SIZE, aesKey);
    if (error != AES_TRUE) {
      fprintf(stderr,"Error creating aes key.\n");
      goto cleanup;
    }
    
    if ((error = aesCipherInit (&cipher, AES_MODE_CBC, aesIv)) != AES_TRUE) {
        fprintf(stderr,"Error initializing cipher.\n");
        goto cleanup;
    }
    
    /* iterate over blocks, decrypting 
     */
    for(i=0; i< DATA_BLK_SIZE; i += CHUNK_SIZE){
      error = aesBlockDecrypt(&cipher, 
			      &key, 
			      cipherTextBytes + i,
			      CHUNK_SIZE*8, 
			      tempblock);
      if (error <= 0) {
	fprintf(stderr,"Failed to decrypt block.\n");
	goto cleanup;
      }
      if((memcmp(tempblock, plainTextBytes + i, CHUNK_SIZE)) !=0){
	fprintf(stderr, "Failed to compare after decryption\n");
	goto cleanup;
      }
    }
    (void) ftime(&tp2);
    printf("encrypt/decrypt compared\n");
    printf("time to decrypt %d bytes = %d \n", DATA_BLK_SIZE,(tp2.time - tp1.time));
      
    /* test of one-shot encryption */
	bsl_aes_sw_encrypt(aesKey, aesIv, plainTextBytes,
			1024, cipherTextBytes);

	bsl_aes_sw_decrypt(aesKey, aesIv, cipherTextBytes,
			1024, tempblock);
	if((memcmp(plainTextBytes, tempblock, 1024)) !=0){
		fprintf(stderr, "Failed to compare after decryption\n");
		goto cleanup;
      	}
	else{
		fprintf(stderr, "encrypt/decrypt compared for one-shot\n");
	}


    
    
    /* do SHA calculation to time
     */
    (void) ftime(&tp1);
    bsl_sha_reset(&sha);
    for(i=0; i < DATA_BLK_SIZE; i += CHUNK_SIZE){
      bsl_sha_input(&sha, plainTextBytes + i, CHUNK_SIZE);
    }
    bsl_sha_result(&sha, hash);
    (void) ftime(&tp2);
    printf("time to hash %d bytes = %d \n", DATA_BLK_SIZE,(tp2.time - tp1.time));
    
    /* test HMAC */
	
    bsl_hmac_sha1(plainTextBytes, 60, hmacKey, 20, hash);
    printf("hmac is->\n");
    for(i=0; i< 20; i++){
	printf("%2x ", hash[i]);
    }
    printf("\n");
    
    return 0;
 cleanup: 
    return -1;

}


