
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <time.h>
#include <bbtypes.h>
#include <bbsec.h>
#include <bsl.h>

#include <poly_math.h>
#include <stdlib.h>


int main(){
    
    BbEccPrivateKey private_key1;
    BbEccPublicKey public_key1;
    BbEccPrivateKey private_key2;
    BbEccPublicKey public_key2;
    BbAesKey shared_key1;
    BbAesKey shared_key2;
    u8 input_string[1024];
    u32 rand_input[8];
    BbEccSig eccsign;
    
    BSL_error result;
    field_2n a,b,c;
    int k;
    struct timeb tp1, tp2;
    
    
    unsigned int seed;
    int i, same;
    int iter =0;
    u32 rand_bit, rand_word, mask;


    seed = getpid();
    srand(seed);

    for(iter = 0; iter < 50; iter++){
    /* simple poly math tests */
      for(i=0; i< 8; i++){
	a.e[i] = rand();
	b.e[i] = rand();
      }
      a.e[0] &= UPR_MASK;
      b.e[0] &= UPR_MASK;
      poly_mul(&a, &b, &c);
    
      poly_inv(&a, &b);
      poly_mul(&a, &b, &c);

      same = 0;
      for(i=0; i< 7; i++){
	if (c.e[i] != 0){
	  same = 1;
	}
      }
      if(c.e[7] != 0x1){
	same = 1;
      }
    
      /* c should be one */
      if(same == 0)
	printf("POLY TEST PASS %d \n", iter);
      else
	printf("POLY TEST FAIL %d \n", iter);
    }



    /* public key and shared key tests */
   

    for(iter = 0; iter < 1; iter++){
    /* random private key */
      for(i=0; i< 8; i++){
	private_key1[i] = rand();
      }
      private_key1[0] &= UPR_MASK;
      
      private_key1[0] = 0x00000179;
      private_key1[1] = 0xcda7e2fe;
      private_key1[2] = 0x16d77e37;
      private_key1[3] = 0x39fbb3de;
      private_key1[4] = 0x618d5d57;
      private_key1[5] = 0x9064949b;
      private_key1[6] = 0x7721c36c;
      private_key1[7] = 0x0a66a2a6;
      
      /* for timing : iterate */
      
      (void) ftime(&tp1);
      for(k = 0; k < 5; k++)
	bsl_ecc_gen_public_key(public_key1, private_key1);

      (void) ftime(&tp2);
      printf("time for 5 keys (s)= %d \n", (tp2.time - tp1.time));
                
      
      /* random private key */
      for(i=0; i< 8; i++){
	private_key2[i] = rand();
      }
      private_key2[0] &= UPR_MASK;

      private_key2[0] = 0x0000017a;
      private_key2[1] = 0xcda7e2fe;
      private_key2[2] = 0x16d77e37;
      private_key2[3] = 0x39fbb3de;
      private_key2[4] = 0x618d5d57;
      private_key2[5] = 0x9064949b;
      private_key2[6] = 0x7721c36c;
      private_key2[7] = 0x0a66a2a6;
      
      

      bsl_ecc_gen_public_key(public_key2, private_key2);

      /* generate the two shared keys */

      /* iterate to time */
      
      (void) ftime(&tp1);
      for(k = 0; k < 5; k++)
	bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      (void) ftime(&tp2);


      printf("time for 5 shared keys (s)= %d \n", (tp2.time - tp1.time));
        
      bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      bsl_ecc_gen_shared_key(public_key2, private_key1, shared_key2);
      same = 0;
      for(i=0; i < sizeof(BbAesKey)/sizeof(u32); i++){
	printf("shared key 1 = %08x 2 = %08x\n", (unsigned int)shared_key1[i], (unsigned int)shared_key2[i]);
	if(shared_key1[i] != shared_key2[i]){
	  same = 1;
	}
      }
      if(same == 0)
	printf("KEY GEN PASS %d \n", iter);
      else
	printf("KEY GEN FAIL %d \n", iter);


      /* negative test, purturb private_key 2 */
      rand_word = ((rand() & 0xff) % 7) + 1;
      rand_bit = rand() & 0xff;
      mask = 0x1 << (rand_bit % 31);
      printf("mask = %08x\n", (unsigned int )mask);
      printf("rand_word= %08x\n", (unsigned int )rand_word);
      private_key2[rand_word] = private_key2[rand_word] ^ mask;
      bsl_ecc_gen_shared_key(public_key1, private_key2, shared_key1);
      same = 0;
      for(i=0; i < sizeof(BbAesKey)/sizeof(u32); i++){
	printf("shared key 1 = %08x 2 = %08x\n", (unsigned int)shared_key1[i], (unsigned int)shared_key2[i]);
	if(shared_key1[i] != shared_key2[i]){
	  same = 1;
	}
      }
      if(same == 1)
	printf("KEY GEN NEGATIVE TEST PASS %d \n", iter);
      else
	printf("KEY GEN NEGATIVE TEST FAIL %d \n", iter);

      
    }
  
    /* one test for zeros in signature, small input */

    for(i = 0; i < 1024; i++){
      input_string[i] = 0x0;
    }
   
	
    for(i=0; i< 8; i++){
      private_key1[i] = rand();
    }
    private_key1[0] &= UPR_MASK;
    
    for(i=0; i< 8; i++){
      rand_input[i] = rand();
    }
    rand_input[0] &= UPR_MASK;
    
    bsl_ecc_compute_sig(input_string, 4, private_key1, rand_input, eccsign, 0x30);
    
    for(i =0; i< 16; i++){
      eccsign[i] = 0x0;
    }
      
    bsl_ecc_gen_public_key(public_key1, private_key1);

    result = bsl_ecc_verify_sig(input_string, 4, public_key1, eccsign,  0x00);
      
    if(result == BSL_OK){
      printf("SIGN VERIFY FAIL ZEROS TEST \n");
    }
    else{
      printf("SIGN VERIFY PASS ZEROS TEST \n");
    }


    /* test the ECDSA part iteratively */
    for(iter = 0; iter < 1; iter++){
      for(i = 0; i < 1024; i++){
        input_string[i] = rand() & 0xff;
      }
    
      for(i=0; i< 8; i++){
	private_key1[i] = rand();
      }
      private_key1[0] &= UPR_MASK;
         
      for(i=0; i< 8; i++){
	rand_input[i] = rand();
      }
      rand_input[0] &= UPR_MASK;
         
      private_key1[0] = 0x00000179;
      private_key1[1] = 0xcda7e2fe;
      private_key1[2] = 0x16d77e37;
      private_key1[3] = 0x39fbb3de;
      private_key1[4] = 0x618d5d57;
      private_key1[5] = 0x9064949b;
      private_key1[6] = 0x7721c36c;
      private_key1[7] = 0x0a66a2a6;
      
       
      rand_input[0] = (0x00000019);
      rand_input[1] = (0xcda7e2fe);
      rand_input[2] = (0x16d77e37);
      rand_input[3] = (0x39fbb3de);
      rand_input[4] = (0x618d5d57);
      rand_input[5] = (0x9064949b);
      rand_input[6] = (0x7721c36c);
      rand_input[7] = (0x0a66a2a6);
      
      /* iterate to time */
      
      (void) ftime(&tp1);
      for(k = 0; k < 5; k++)
	bsl_ecc_compute_sig(input_string, 1024, private_key1, rand_input, eccsign, 0x30);
      (void) ftime(&tp2);

      printf("time for 5 signs (s)= %d \n", (tp2.time - tp1.time));
        
      bsl_ecc_compute_sig(input_string, 1024, private_key1, rand_input, eccsign, 0x30);

      
      bsl_ecc_gen_public_key(public_key1, private_key1);


      (void) ftime(&tp1);
      for(k = 0; k < 5; k++)
	result = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign, 0x30);
      (void) ftime(&tp2);

      printf("time for 5 verifies (s)= %d \n", (tp2.time - tp1.time));
        
      result = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign, 0x30);
    
      
      if(result == BSL_OK){
        printf("SIGN VERIFY PASS %d\n", iter);
      }
      else{
        printf("SIGN VERIFY FAIL %d\n", iter);
      }
      /* change a bit */
      rand_word = rand() & 0x7;
      rand_bit = rand() & 0x7f;
      mask = 0x1 << (rand_bit % 32);
      eccsign[rand_word] = eccsign[rand_word] ^ mask;
      
      bsl_ecc_gen_public_key(public_key1, private_key1);
      result = bsl_ecc_verify_sig(input_string, 1024, public_key1, eccsign,  0x30);
    
      printf("rand_word = %ld\n", rand_word);
      printf("rand_bit = %ld\n", rand_bit);
      printf("mask = %08x\n", (unsigned int )mask);

      
      if(result != BSL_OK){
        printf("SIGN VERIFY NEG TEST PASS %d\n", iter);
      }
      else{
        printf("SIGN VERIFY NEG TEST FAIL %d\n", iter);
      }
      
    }
        
    
    return 0;
}


