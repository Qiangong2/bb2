/* 
 * author padraigo
 * synopsis
 * download an srec into sdram memory and run it
 * -i initialise ebiu
 * -s start address to start executing from
 * -f filename
 */

#include <asm/semaphore.h>
#include <sys/ioctl.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h> 
#include <netinet/in.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include <asm/semaphore.h>
#include <sys/ioctl.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/ejtag.h>

#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

// #include "server.h"
#include "ejtag_primitives.h"
#include "dsu.h"
#include "mercury_ebiu.h"
#include "mercury_pio.h"

/*
 * Just a general comment. If a definition doen not specify 32 bit or 16 bit it is
 * 32 bit. All 16 bit definitons have a 16 in them somewhere.
 */

#define GeneralConfigurationValue Ebiu_Control_Hidden_Agr_Enable  |\
				Ebiu_Control_Sync_Enable     |\
				Ebiu_Control_Enrfsh0_Enable  |\
				Ebiu_Control_Enrfsh1_Enable  |\
				Ebiu_Control_Rfshval_2Rate |\
				Ebiu_Control_Tmips_CAS_Enable

#define GeneralConfigurationValue_EnableRfsh0 Ebiu_Control_Hidden_Agr_Enable |\
				Ebiu_Control_Sync_Enable     |\
				Ebiu_Control_Enrfsh0_Enable  |\
				Ebiu_Control_Rfshval_2Rate |\
				Ebiu_Control_Tmips_CAS_Enable

#define GeneralConfigurationValue_EnableRfsh1 Ebiu_Control_Hidden_Agr_Enable |\
				Ebiu_Control_Sync_Enable     |\
				Ebiu_Control_Enrfsh1_Enable  |\
				Ebiu_Control_Rfshval_2Rate |\
				Ebiu_Control_Tmips_CAS_Enable

#define GeneralConfigurationValueSDRAMInit Ebiu_Control_Hidden_Agr_Enable |\
				Ebiu_Control_Sync_Enable  |\
				Ebiu_Control_Tmips_CAS_Enable


#define SDramBank0ConfigurationValue  Ebiu_Dram_Dtyp_1Type  |\
				Ebiu_Dram_BusWidth_1Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_CasLatency_3Clock |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Nras_2Clock

#define SDramBank0ConfigurationValue2x  Ebiu_Dram_Dtyp_1Type  |\
				Ebiu_Dram_BusWidth_1Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_CasLatency_4Clock |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Nras_2Clock

#define SDramBank1ConfigurationValue  Ebiu_Dram_Dtyp_1Type  |\
				Ebiu_Dram_BusWidth_1Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_CasLatency_3Clock |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Nras_2Clock

#define SDramBank1ConfigurationValue2x  Ebiu_Dram_Dtyp_1Type  |\
				Ebiu_Dram_BusWidth_1Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_CasLatency_4Clock |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Nras_2Clock

#define SDram16Bank1ConfigurationValue  Ebiu_Dram_Dtyp_1Type |\
				Ebiu_Dram_BusWidth_0Width    |\
				Ebiu_Dram_ColumnSelect_0          |\
				Ebiu_Dram_CasLatency_3Clock  |\
				Ebiu_Dram_Nrp_2Clock         |\
				Ebiu_Dram_Nrcd_3Clock        |\
				Ebiu_Dram_Nras_2Clock

#define SDram16Bank0ConfigurationValue  Ebiu_Dram_Dtyp_1Type |\
				Ebiu_Dram_BusWidth_0Width    |\
				Ebiu_Dram_ColumnSelect_0          |\
				Ebiu_Dram_CasLatency_3Clock  |\
				Ebiu_Dram_Nrp_2Clock         |\
				Ebiu_Dram_Nrcd_3Clock        |\
				Ebiu_Dram_Nras_2Clock

#define DramBank0ConfigurationValue  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_1Width |\
				Ebiu_Dram_ColumnSelect_0       |\
				Ebiu_Dram_Nrp_2Clock      |\
				Ebiu_Dram_Nrcd_3Clock     |\
				Ebiu_Dram_Ncp_2Clock      |\
				Ebiu_Dram_Ncas_2Clock     |\
				Ebiu_Dram_Nras_2Clock

#define DramBank0ConfigurationValue_10x10  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_1Width |\
				Ebiu_Dram_ColumnSelect_2       |\
				Ebiu_Dram_Nrp_2Clock      |\
				Ebiu_Dram_Nrcd_3Clock     |\
				Ebiu_Dram_Ncp_2Clock      |\
				Ebiu_Dram_Ncas_2Clock     |\
				Ebiu_Dram_Nras_2Clock

#define DramBank1ConfigurationValue_10x10  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_1Width |\
				Ebiu_Dram_ColumnSelect_2       |\
				Ebiu_Dram_Nrp_2Clock      |\
				Ebiu_Dram_Nrcd_3Clock     |\
				Ebiu_Dram_Ncp_2Clock      |\
				Ebiu_Dram_Ncas_2Clock     |\
				Ebiu_Dram_Nras_2Clock

#define DramBank1ConfigurationValue  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_1Width |\
				Ebiu_Dram_ColumnSelect_0       |\
				Ebiu_Dram_Nrp_2Clock      |\
				Ebiu_Dram_Nrcd_3Clock     |\
				Ebiu_Dram_Ncp_3Clock      |\
				Ebiu_Dram_Ncas_3Clock     |\
				Ebiu_Dram_Nras_3Clock
		
#define Dram16Bank0ConfigurationValue  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_0Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Ncp_2Clock        |\
				Ebiu_Dram_Ncas_2Clock       |\
				Ebiu_Dram_Nras_2Clock

#define Dram16Bank1ConfigurationValue  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_0Width   |\
				Ebiu_Dram_ColumnSelect_0         |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Ncp_2Clock        |\
				Ebiu_Dram_Ncas_2Clock       |\
				Ebiu_Dram_Nras_2Clock
			

#define Dram16Bank0ConfigurationValue_10x10  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_0Width   |\
				Ebiu_Dram_ColumnSelect_2         |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Ncp_2Clock        |\
				Ebiu_Dram_Ncas_2Clock       |\
				Ebiu_Dram_Nras_2Clock

#define Dram16Bank1ConfigurationValue_10x10  Ebiu_Dram_Dtyp_0Type |\
				Ebiu_Dram_BusWidth_0Width   |\
				Ebiu_Dram_ColumnSelect_2         |\
				Ebiu_Dram_Nrp_2Clock        |\
				Ebiu_Dram_Nrcd_3Clock       |\
				Ebiu_Dram_Ncp_2Clock        |\
				Ebiu_Dram_Ncas_2Clock       |\
				Ebiu_Dram_Nras_2Clock
			
		

#define SDRAM_Bank0_Mode_BaseAddress	0xB0900000
#define SDRAM_Bank1_Mode_BaseAddress	0xB0A00000
#define SDRAM_Mode_Register		0x0	
#define	SDRAM_ModeWord1			0x80000400
#define	SDRAM_ModeWord2			0x33
#define	SDRAM_Wait			0x4
#define SDRAM_Mode_Set			0x7
#define SDRAM_Refresh_Wait		100000

#define HM5216165_ModeWord1		0x80000400
#define HM5216165_ModeWord2		0x33

#define BUFSIZE (4*1024)

void ebiu_init(int fd);
void swap_mems(int fd);
void exc_from(int fd,unsigned long sa);

extern unsigned int big_probe_mem[];
int display = 1 ;

void loadimage(int fd, char *filename);

int main(int argc, char **argv)
{
  int fd ;
  char c = 'q'  ;
  unsigned char filename[1024];
  unsigned long startaddr=0xbfc00000;
  int initEbiu=0,load=0,addr=0, verbose=0, swap=0;
  unsigned long impl,dev_id;

  /* process the options */
  while(1)
    {
      c=getopt(argc,argv,"hisvb:f:");

      if ( c == -1 )
	break;

      switch(c)
	{
	case 'b':
	  sscanf(optarg,"%x",&startaddr);
	  printf("start addr : %08x\n",startaddr);
	  addr=1;
	  break ;
	case 'f':
	  sscanf(optarg,"%s",filename);
	  load=1;
	  break;
	case 'i':
	  initEbiu=1;
	  break;
	case 'v':
	  verbose=1;
	  break;
	case 's':
	  swap=1;
	  break;
	default:
	  printf("unknown option %c\n", c);
	case 'h':
	  printf("Usage %s [-h] [-i] [-s num]\n",argv[0]);
	  printf("      -h           print this message\n");
	  printf("      -i           initialise ebiu\n");
	  printf("      -s           swap rom and ram\n");
	  printf("      -v           verbose\n");
	  printf("      -b hexnum    address to start executing from\n");
	  printf("      -f filenanme srec filename\n");
 
	  exit(0);
	}
    }

  /*
  ** open the ejtag, hopefully it's not busy
  */
  fd = open("/dev/ejtag0", O_RDWR|O_SYNC);
  if ( fd < 0 )
    {
      perror("Cannot open /dev/ejtag0");
      close(fd);
      exit(1);
    }

  ioctl(fd,EJTAG_TAPRESET,0);
  mips_ejtag_init(fd,&impl,&dev_id);

  /* stop cpu while initialising the ebiu */
  printf("issuing jtag break\n");
  mips_ejtag_jtagbrk(fd);
  
  if ( initEbiu )
    ebiu_init(fd);

  if ( swap )
    swap_mems(fd);

  if ( load) {
    loadimage(fd,filename);
  } else
    printf("no srec specified\n");

  if ( !addr )
    {
      printf("no start addr specified, assuming %08x\n",startaddr);
    }
  exc_from(fd,startaddr);

  printf("run cpu, be free!\n");
  while( mips_ejtag_checkstatus(fd) & PRACC )
    mips_ejtag_release(fd);
}

void loadimage(int fd, char *filename)
{
  time_t begin=0, now=0, prev=0 ;
  unsigned cnt, bytes=0 ;
  int srec_fd ;
  char buf[BUFSIZE];
  int iter ;

  time(&begin);
  srec_fd=open(filename, O_RDONLY);
  if (srec_fd<=0) {
    fprintf(stderr,"Could not open %s for reading\n", filename);
    exit(1);
  }
  
  while ( (cnt=read(srec_fd,buf,BUFSIZE)) ) {
    write(fd,buf,cnt);
    bytes+=cnt ;
    time(&now);
    now-=begin;
    if ( now ) {
      if ( prev != now ) {
	prev=now;
	printf("time %4d bytes %8d %14f bytes/sec\n", now, bytes, (float)bytes/(float)(now));
      }
    } else {
      printf("too fast to determine\n");
    }
  }

  close(srec_fd);
}

void swap_mems(int fd)
{
  unsigned long addr, odata, data;
  /* now get the ebiu to swap the boot device with SDRAM */
  addr=Ebiu_BaseAddress |Ebiu_GeneralConfigurationRegister;
  odata=mips_ejtag_read_w(fd,addr);
  data=odata^Ebiu_Control_Swap_CS0_BK0;
  mips_ejtag_write_w(fd,addr,data);
}

void ebiu_init(int fd)
{
  unsigned long rv, odata,data, addr ;


  printf("initialising ebiu\n");
  addr = Pio_BaseAddress + Pio_StrapRegister ;
  data = mips_ejtag_read_w(fd,addr);

  if ( data&0x4 )
    { /* 2x */
      printf("2x EBIU Timing Settings\n");
      data = (0xf<< 28)|(0x1f<<23)|(0x1f<<18)|(0x1f<<13)|(0x1f<<8)|(0x1<<6)|(0x1<<5);
      addr= Ebiu_BaseAddress | Ebiu_Cs0ConfigurationRegister ;
      odata=mips_ejtag_read_w(fd,addr);
      mips_ejtag_write_w(fd,addr,data);
      rv=mips_ejtag_read_w(fd,addr);
      printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

      addr=Ebiu_BaseAddress|Ebiu_ClockControlRegister ;
      odata = mips_ejtag_read_w(fd,addr);
      data = odata|Ebiu_Invert_External_clock ;
      data &= ~Ebiu_internal_delay_mask ;
      mips_ejtag_write_w(fd,addr,data);
      rv=mips_ejtag_read_w(fd,addr);
      printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);
    }
  else
    { /* 1x */
      printf("1x EBIU Timing Settings\n");
      data = (0xf<< 28)|(0x10<<23)|(0x10<<18)|(0x10<<13)|(0x10<<8)|(0x1<<6)|(0x1<<5);
      addr= Ebiu_BaseAddress | Ebiu_Cs0ConfigurationRegister ;
      odata=mips_ejtag_read_w(fd,addr);
      mips_ejtag_write_w(fd,addr,data);
      rv=mips_ejtag_read_w(fd,addr);
      printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

      addr=Ebiu_BaseAddress|Ebiu_ClockControlRegister ;
      odata = mips_ejtag_read_w(fd,addr);
      data = odata|Ebiu_Invert_External_clock ;
      mips_ejtag_write_w(fd,addr,data);
      rv=mips_ejtag_read_w(fd,addr);
      printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);
    }

  /* Init the ebiu for 32 bit SDRAM in bank 0. */
  addr=Ebiu_BaseAddress|Ebiu_GeneralConfigurationRegister;
  data=GeneralConfigurationValueSDRAMInit ;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

  addr=Ebiu_BaseAddress|Ebiu_DramBank0ConfigurationRegister;
  data=SDramBank0ConfigurationValue;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

  addr=Ebiu_BaseAddress|Ebiu_Mode0ConfigurationRegister;
  data=SDRAM_Mode_Set;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);
      
  /* For SDRAM we must write to the SDRAM Mode register to initalize it. */
  addr=SDRAM_Bank0_Mode_BaseAddress|SDRAM_Mode_Register;
  data=HM5216165_ModeWord1;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

  /* Set the general config register with refresh enabled! */
  addr=Ebiu_BaseAddress|Ebiu_GeneralConfigurationRegister;
  data=GeneralConfigurationValue_EnableRfsh0;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

  /* Wait a for a SDRAM & DRAM refresh. */
  for(addr=0;addr<1000000;addr++);

  addr=SDRAM_Bank0_Mode_BaseAddress|SDRAM_Mode_Register;
  data=HM5216165_ModeWord2;
  odata=mips_ejtag_read_w(fd,addr);
  mips_ejtag_write_w(fd,addr,data);
  rv=mips_ejtag_read_w(fd,addr);
  printf("%08x reads %08x after writing %08x orig %08x\n", addr,rv,data,odata);

  /* Wait a for a SDRAM & DRAM refresh. */
  for(addr=0;addr<1000000;addr++);
}

void exc_from(int fd,unsigned long sa)
{
  unsigned ctrl ;
  unsigned addr ;
  unsigned pc ;

  // these instructions will not be executed by the cpu
  mips_ejtag_pracc(fd,0x3c040000|(sa>>16));     // lui     $a0,%hi(sa)
  mips_ejtag_pracc(fd,0x34840000|(sa&0xffff));  // ori     $a0,%lo(sa) 
  mips_ejtag_pracc(fd,0x40848800);              // mtc0    $a0,$lladdr 
  mips_ejtag_pracc(fd,NOP);                     // nop                 
  printf ("Set EPC to       %08x\n", sa);
}

