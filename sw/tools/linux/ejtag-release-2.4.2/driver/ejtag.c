/*
 * EJTAG parallel port driver
 *
 * Copyright (C) 2000 padraigo
 *  Based on ejtag.c
 */

#include <linux/module.h>
#include <linux/init.h>

#include <linux/config.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/sched.h>
#include <linux/smp_lock.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/slab.h>
#include <linux/fcntl.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/console.h>

#include <linux/parport.h>
#include <ejtag.h>

#include <asm/irq.h>
#include <asm/uaccess.h>
#include <asm/system.h>

/* include the following two to deal with 
 * TCGETS, TCSETS & TCFLSH from gdb*/
#include <asm/ioctls.h>
#include <asm/termbits.h>

#define EJT_DELAY 10
#define EJTAG_MAJOR 200

/* if you have more than 3 ejtags, remember to increase EJTAG_NO */
#define EJTAG_NO 3

struct ejtag_struct ejtag_table[EJTAG_NO] =
{
  [0 ... EJTAG_NO-1] = {
    dev             : NULL,                 /* dev */
    flags           : 0,                    
    time            : EJTAG_INIT_TIME,      
    wait            : EJTAG_INIT_DEBUG,     
    bufout          : NULL,                 /* bufin */
    bufin           : NULL,                 /* bufout */
    ejtag_probe_mem : NULL,                 
    ctrl            : 0,                    
    instr           : 0 }
};

/******************************************************************************
** routines dealing with low level ejtag functionality
******************************************************************************/
static inline void pp_write_data(int minor, unsigned char data);
static inline void pp_write_ctrl(int minor, unsigned char ctrl);
static inline int pp_read_stat(int minor);
static void ejt_reset(int minor);
static inline unsigned char ejt_tapmove(int minor,
					unsigned char tms, unsigned char tdo);
static void ejt_tapreset(int minor);
static unsigned int ejt_data(int minor,unsigned din);
static unsigned char ejt_instr(int minor,unsigned char din);
static unsigned int ejt_dmawrite_w(int minor,
				   unsigned int addr,unsigned int data);
static unsigned int ejt_dmawrite_h(int minor,
				   unsigned int addr,unsigned int data);
static unsigned int ejt_dmawrite_b(int minor,
				   unsigned int addr,unsigned int data);
static unsigned int ejt_dmaread_w(int minor,unsigned int addr);
static unsigned int ejt_dmaread_h(int minor,unsigned int addr);
static unsigned int ejt_dmaread_b(int minor,unsigned int addr);
static unsigned int ejt_version(int minor);
static unsigned int ejt_implementation(int minor);
static unsigned int ejt_checkstatus(int minor);
/******************************************************************************
 * routines dealing with buffer management
 *****************************************************************************/
static void ejtag_buf_init(ejtag_bufm_t *pb);
static inline void ejtag_buf_copy(ejtag_bufm_t *pb, char*src, int len);
static inline int ejtag_buf_write(ejtag_bufm_t *pb, char*src, int len);
static inline int ejtag_buf_empty(ejtag_bufm_t *pb);
static inline int ejtag_buf_full(ejtag_bufm_t *pb);
static inline int ejtag_buf_getChar(ejtag_bufm_t *pb, char*c) ;
static inline int ejtag_buf_getNybble(ejtag_bufm_t *pb, int*n);
static inline int ejtag_buf_srecGetByte(ejtag_bufm_t *pb, int*b);
static inline int ejtag_buf_srecGetWord(ejtag_bufm_t *pb, int *w);
static inline int ejtag_buf_srecGet(ejtag_bufm_t *pb);
static int ejtag_buf_loadSrec(int minor);

#undef EJTAG_DEBUG
#undef EJTAG_READ_DEBUG

#define ejtag_parport_release(x)	do { parport_release(ejtag_table[(x)].dev); } while (0);
#define ejtag_parport_claim(x)	do { parport_claim_or_block(ejtag_table[(x)].dev); } while (0);

extern int dbg ;
static devfs_handle_t devfs_handle = NULL;
static unsigned int ejtag_count = 0;

/*
 * ROUTINE    : ejtag_buf_init
 * INPUTS     :  pb - pointer to producer/consumer buffer
 *               suspend - callback
 * OUTPUTS    : None
 * RETURNS    : None
 * DESCRIPTION: initialise the ejtag buffer system
 */
static void ejtag_buf_init(ejtag_bufm_t *pb)
{
  pb->state=Start;
  pb->wptr=0;
  pb->rptr=0;
  pb->left=EJTAGBUFSIZE ;
  pb->eob=0 ;
  pb->bytes=0;
}

/*
 * ROUTINE    : ejtag_buf_copy
 * INPUTS     :  pb - pointer to producer/consumer buffer
 *               src - pointer to source buffer to copy
 *               len - length of buffer to copy
 * OUTPUTS    : None
 * RETURNS    : None
 * DESCRIPTION: copy data contents into circular buffer taking into account
 * the circular nature of the buffer
 */
static inline void ejtag_buf_copy(ejtag_bufm_t *pb, char*src, int len)
{
  int firstchunk,lastchunk ;

#if 0    
  printk(KERN_DEBUG "copying %d bytes state %d\n", len, pb->state );
  printk(KERN_DEBUG "wptr %4d rptr %4d left %4d bytes %d\n", 
	 pb->wptr, pb->rptr, pb->left, pb->bytes);
#endif

  /* check for wrap around */
  firstchunk=EJTAGBUFSIZE-pb->wptr;
  if ( firstchunk < len ) {
    copy_from_user(pb->buf+pb->wptr, src, firstchunk);
    lastchunk=len-firstchunk ;
    copy_from_user(pb->buf,src+firstchunk,lastchunk);
  } else {
    copy_from_user(pb->buf+pb->wptr, src, len);
  }
  pb->wptr=(pb->wptr+len)%EJTAGBUFSIZE ;
  pb->left-=len;
  pb->bytes+=len ;
}

/*
 * ROUTINE    : ejtag_buf_copy
 * INPUTS     :  pb - pointer to producer/consumer buffer
 *               src - pointer to source buffer to copy
 *               len - length of buffer to copy
 * OUTPUTS    : None
 * RETURNS    : None
 * DESCRIPTION: write data into buffer, return size of left over if whole 
 * amount cannot be copied in 
 */
static inline int ejtag_buf_write(ejtag_bufm_t *pb, char*src, int len)
{
  int cpysize ;

  /* determine number of bytes to copy */
  cpysize = len > pb->left ? pb->left : len ;

  /* determine number of left over bits if any */
  ejtag_buf_copy(pb,src,cpysize);
  return cpysize ;
}

/*
 * ROUTINE    : ejtag_buf_empty
 * INPUTS     :  pb - pointer to producer/consumer buffer
 * OUTPUTS    : None
 * RETURNS    : true/false 
 * DESCRIPTION: return whether buffer is empty or not
 */
static inline int ejtag_buf_empty(ejtag_bufm_t *pb)
{
  return pb->left==EJTAGBUFSIZE ;
}

/*
 * ROUTINE    : ejtag_buf_full
 * INPUTS     :  pb - pointer to producer/consumer buffer
 * OUTPUTS    : None
 * RETURNS    : true/false 
 * DESCRIPTION: return whether buffer is full
 */
static inline int ejtag_buf_full(ejtag_bufm_t *pb)
{
  return pb->left==0 ;
}

/*
 * ROUTINE    : ejtag_buf_getChar
 * INPUTS     :  pb - pointer to producer/consumer buffer
 *               c destination of character
 * OUTPUTS    : None
 * RETURNS    : int -ve buffer underrun 0 char valid
 * DESCRIPTION: return next character from buffer.
 */
static inline int ejtag_buf_getChar(ejtag_bufm_t *pb, char*c) 
{
  if ( ejtag_buf_empty(pb) )
    return -1 ;

  *c = pb->buf[pb->rptr];
  pb->rptr=(pb->rptr+1)%EJTAGBUFSIZE;
  pb->left++;
  return 0 ;
}

/*
 * ROUTINE    : ejtag_buf_getNybble
 * INPUTS     :  pb - pointer to producer/consumer buffer 
 * OUTPUTS    : None
 * RETURNS    : int
 * DESCRIPTION: return nybble from character in buffer
 */
static inline int ejtag_buf_getNybble(ejtag_bufm_t *pb, int *n)
{
  if ( ejtag_buf_getChar(pb,(char*)n) )
    return -1 ;

  if (*n>='0' && *n <= '9' )
    *n = *n-'0' ;
  else if ( *n>='a' && *n <= 'f' )
    *n = *n-'a'+10 ;
  else if ( *n>='A' && *n <= 'F' )
    *n = *n-'A'+10 ;

  return 0 ;
}

/*
 * ROUTINE    : ejtag_buf_srecGetByte
 * INPUTS     :  pb - pointer to producer/consumer buffer 
 * OUTPUTS    : None
 * RETURNS    : int
 * DESCRIPTION: return byte from character in buffer
 */
static inline int ejtag_buf_srecGetByte(ejtag_bufm_t *pb, int *b)
{
  int i, t ;

  /* check there are enough characters in the buffer */
  if ( (EJTAGBUFSIZE - pb->left) < 2 ) return -1 ;

  /* derive number of bytes in the string */
  for(*b=0,t=0,i=0;i<2;i++) {
    if (ejtag_buf_getNybble(pb,&t) ) {
      printk(KERN_DEBUG "ejtag : buffer underrun\n");
      return -1 ;
    }
    *b=t+*b*16 ;
  }
  return 0 ;
}

/*
 * ROUTINE    : ejtag_buf_srecGetWord
 * INPUTS     :  pb - pointer to producer/consumer buffer 
 * OUTPUTS    : None
 * RETURNS    : int
 * DESCRIPTION: return word from character in buffer
 */
static inline int ejtag_buf_srecGetWord(ejtag_bufm_t *pb, int *w)
{
  int t,i ;

  /* check there are enough characters in the buffer */
  if ( EJTAGBUFSIZE - pb->left < 8 ) return -1 ;

  for(*w=0,t=0,i=0;i<8;i++) {
    if ( ejtag_buf_getNybble(pb,&t) ) {
      printk(KERN_DEBUG "ejtag : buffer underrun\n");
      return -1 ;
    }
    *w=*w*16+t ;
  }
  return 0 ;
}

/*
 * ROUTINE    : ejtag_buf_srecGet
 * INPUTS     :  pb - pointer to producer/consumer buffer 
 * OUTPUTS    : None
 * RETURNS    : int
 * DESCRIPTION: set up ejtag_buf so internal rptr points 
 *              to the next srec (it can find!)
 */
static inline int ejtag_buf_srecGet(ejtag_bufm_t *pb)
{
  char c ;

  c=0;
  while ( c != 'S' ) { 
    if ( ejtag_buf_getChar(pb,&c ) )
      return -1 ;
  }
  return 0 ;
}

/*
 * ROUTINE    : ejtag_buf_loadSrec
 * INPUTS     :  ejt - pointer to ejtag structure
 * OUTPUTS    : None
 * RETURNS    : int
 * DESCRIPTION: read srec line and write it to ejtag.
 *   implement a state machine to keep track of where
 *   we are in writing the srec
 */
static int ejtag_buf_loadSrec(int minor)
{
  ejtag_bufm_t *pb=ejtag_table[minor].bufout ;

  while(1)
    switch(pb->state) {
    case Start: {
      if ( ejtag_buf_srecGet(pb) ) 
	return -1 ;
      pb->state=Type ;
    } /* Start */

    case Type: {
      if ( ejtag_buf_getChar(pb, &pb->srecType) )
	return -1 ;
      pb->state=Count ;
    } /* Type */

    case Count: {
      if ( ejtag_buf_srecGetByte(pb,&pb->srecCount) )
	return -1 ;
      pb->srecCount -=1 ; /* processed the len, 1 byte consumed */
      pb->state=Addr ;
    } /* Count */

    case Addr: {
      if ( ejtag_buf_srecGetWord(pb,&pb->srecAddr) )
	return -1 ;
      pb->srecCount -= 4 ; /* processed the address, 4 bytes consumed */
      pb->srecAddr&=0x1fffffff ;
      switch(pb->srecType) {
      case 3: { 
	pb->state=Data ; 
      } /* 3 */
      case 7: { 
	pb->state=ChkSum ; 
	  printk(KERN_DEBUG "SREC says to start at %x\n", (unsigned)pb->srecAddr);
	  break ;
      } /* 3 */
      }
    } /* Addr */

    case Data: {
      while(pb->srecCount) {
	if ( ejtag_buf_srecGetWord(pb,&pb->srecData) )
	  return -1 ;
#if 0
	printk("ejtag%d : wr %08x %08x\n", minor,pb->srecAddr,pb->srecData);
#endif
	ejt_dmawrite_w(minor,pb->srecAddr,pb->srecData);
	pb->srecAddr+=4 ;
	pb->srecCount-=4 ;
      }
      pb->state=ChkSum ; 
    } /* Data */

    case ChkSum: {
      /* read and discard ! */
      ejtag_buf_srecGetByte(pb,&pb->srecCount);
      pb->state=Start ; 
  
    } /* ChkSum */

    } /* switch( .. ) */
  return 0 ;
}

/* --- low-level parport interface routines -------------------------- */

/* write data to the parallel port */
static void pp_write_data(int minor, unsigned char data)
{
  struct parport *p = ejtag_table[minor].dev->port;
  parport_write_data(p, data); 
}

/* write ctrl to the parallel port */
static void pp_write_ctrl(int minor, unsigned char ctrl)
{
  struct parport *p = ejtag_table[minor].dev->port;
  parport_write_control(p, ctrl); 
}

/* read in from the busy line of the parallel port */
static int pp_read_stat(int minor)
{
  struct parport *p = ejtag_table[minor].dev->port;
  return parport_read_status(p) ;
}

/* --- low-level EJTAG functions ----------------------------------- */

/* reset the tap controller */
static void ejt_reset(int minor)
{
  unsigned char dout ;

  dout = (unsigned char)TRSTN_BIT(0);
  pp_write_data(minor,dout);
  dout = (unsigned char)TRSTN_BIT(1);
  pp_write_data(minor,dout);
  ejtag_table[minor].ctrl=0;
  ejtag_table[minor].instr=0;
  ejtag_table[minor].debug=0;
}

/* move the tap controller to another state */
static unsigned char ejt_tapmove(int minor,unsigned char tms, unsigned char tdo)
{
    unsigned char dout,din ;
    dout = (unsigned char) (TRSTN_BIT(1) | TMS_BIT(tms) | TDO_BIT(tdo) | TCK_BIT(0));
    pp_write_data(minor,dout);
    dout = (unsigned char) (TRSTN_BIT(1) | TMS_BIT(tms) | TDO_BIT(tdo) | TCK_BIT(1));
    pp_write_data(minor,dout);
#ifdef BB_DEBUG_BOARD
    din=pp_read_stat(minor)&PARPORT_STATUS_PAPEROUT ? 1 : 0;
#else
    din=pp_read_stat(minor)&PARPORT_STATUS_BUSY ? 0 : 1;
#endif
    dout = (unsigned char) (TRSTN_BIT(1) | TMS_BIT(tms) | TDO_BIT(tdo) | TCK_BIT(0));
    pp_write_data(minor,dout);
    return din;
}

/* reset the tap controller */
static void ejt_tapreset(int minor)
{
  int i ;
  unsigned char dout ;
  for (i=0;i<5;i++)
    {
      dout = TRSTN_BIT(1) | TMS_BIT(1) | TDO_BIT(0) | TCK_BIT(0);
      pp_write_data(minor,dout);
      dout = TRSTN_BIT(1) | TMS_BIT(1) | TDO_BIT(0) | TCK_BIT(1);
      pp_write_data(minor,dout);
    }
}

/* scan data into the tap controller's data register */
static unsigned int ejt_data(int minor,unsigned din)
{
  int i ;
  unsigned dout ;
  unsigned char bit ;
  unsigned ctrl_tx ;

  ctrl_tx=din ;

  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,1,0);             /* enter select-dr-scan    */
  ejt_tapmove(minor,0,0);             /* enter capture-dir       */
  ejt_tapmove(minor,0,0);             /* enter shift-dr          */

  dout=0;
  for(i=0;i<31;i++)
    {
      bit=ejt_tapmove(minor,0,din&0x1); /* enter shift-dr          */
      din=din>>1;
      dout=dout|((bit&0x1)<<i);
    }
  bit=ejt_tapmove(minor,1,din&0x1);   /* enter shift-dr          */
  dout=dout|((bit&0x1)<<i);

  ejt_tapmove(minor,1,0);             /* enter update-dr         */
  ejt_tapmove(minor,0,0);             /* enter runtest-idle      */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */

  if ( ejtag_table[minor].instr == JTAG_CONTROL_IR )
    {
      ejtag_table[minor].ctrl = dout ;
      if ( ejtag_table[minor].debug )
	printk(KERN_DEBUG "ejtag%d: ctrl_tx %08x ctrl_rx %08x\n", minor,ctrl_tx,dout);
      
    }

  return dout ;
}

/* scan data into the tap controller's instruction register */
static unsigned char ejt_instr(int minor,unsigned char din)
{
  unsigned dout ;
  unsigned char bit ;

  /* keep track of last instruction */

  ejtag_table[minor].instr = din ;

  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,1,0);             /* enter select-dr-scan    */
  ejt_tapmove(minor,1,0);             /* enter select-ir-scan    */
  ejt_tapmove(minor,0,0);             /* enter capture-ir        */
  ejt_tapmove(minor,0,0);             /* enter shift-ir (dummy)  */

  dout=0;
  bit=ejt_tapmove(minor,0,din&0x1);   /* enter shift-ir */
  din=din>>1;
  dout=dout|((bit&0x1)<<0);
  bit=ejt_tapmove(minor,0,din&0x1);   /* enter shift-ir */
  din=din>>1;
  dout=dout|((bit&0x1)<<1);
  bit=ejt_tapmove(minor,0,din&0x1);   /* enter shift-ir */
  din=din>>1;
  dout=dout|((bit&0x1)<<2);
  bit=ejt_tapmove(minor,0,din&0x1);   /* enter shift-ir */
  din=din>>1;
  dout=dout|((bit&0x1)<<3);
  bit=ejt_tapmove(minor,1,din&0x1);   /* enter shift-ir */
  dout=dout|((bit&0x1)<<4);

  ejt_tapmove(minor,1,0);             /* enter update-ir */
  ejt_tapmove(minor,0,0);             /* enter runtest-idle */
  ejt_tapmove(minor,0,0);             /* runtestidle             */
  ejt_tapmove(minor,0,0);             /* runtestidle             */

  return dout ;
}

/* scan in the instructions to do a pi word write */
static unsigned int ejt_dmawrite_w(int minor,unsigned int addr,unsigned int data)
{
  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* scan in data */
  ejt_instr(minor,JTAG_DATA_IR);
  ejt_data(minor,data);

  /* set up ctrl reg to do a DMA word write, no checks for DSTRT since this interface
   * is very slow! 
   */

   /* clear old DMA size and any read/lock flags */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DRWN|DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK);
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_WORD|TOF|PRACC ;
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_w with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_w returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* clear the DMAACC after write as per specification, could interfere with PRACC! */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_WORD ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();
  return ejtag_table[minor].ctrl;
}

/* scan in the instructions to do a pi halfword write */
static unsigned int ejt_dmawrite_h(int minor,unsigned int addr,unsigned int data)
{
  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* handle the bigendian/littleendian */
  data = (data&0x0000ffff) | (data<<16) ;

  /* scan in data */
  ejt_instr(minor,JTAG_DATA_IR);
  ejt_data(minor,data);

  /* set up ctrl reg to do a DMA word write, no checks for DSTRT since this interface
   * is very slow! 
   */

   /* clear old DMA size and any read/lock flags */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DRWN|DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK);
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_HALFWORD|TOF|PRACC ;
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_h with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_h returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* clear the DMAACC after write as per specification, could interfere with PRACC! */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_HALFWORD ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();
  return ejtag_table[minor].ctrl;
}

/* scan in the instructions to do a pi byte write */
static unsigned int ejt_dmawrite_b(int minor,unsigned int addr,unsigned int data)
{

  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* handle the bigendian/littleendian */
  data = data & 0xff ;
  data = (data<<24)|(data<<16)|(data<<8)|data ;
  /* scan in data */
  ejt_instr(minor,JTAG_DATA_IR);
  ejt_data(minor,data);

  /* set up ctrl reg to do a DMA word write, no checks for DSTRT since this interface
   * is very slow! 
   */

   /* clear old DMA size and any read/lock flags */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DRWN|DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK);
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_BYTE|TOF|PRACC ;
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_b with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmawrite_b returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* clear the DMAACC after write as per specification, could interfere with PRACC! */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_BYTE ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();
  return ejtag_table[minor].ctrl;
}

/* scan in the instructions to do a pi word read */
static unsigned int ejt_dmaread_w(int minor,unsigned int addr)
{
  unsigned int data ;

  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* set up ctrl reg to do a DMA word read, no checks for DSTRT since this interface
   * is very slow!
   */
  /* clear old DMA size,lock flag, proc & periph resets */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_WORD|DRWN|TOF|PRACC ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl=ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* no more transmissions */
  ejtag_table[minor].ctrl &= ~DSTRT;
  
  /* read back data */
  ejt_instr(minor,JTAG_DATA_IR);
  data = ejt_data(minor,0);

  /* clear DMACC */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_WORD;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();
  return data ;
}

/* scan in the instructions to do a pi word read */
static unsigned int ejt_dmaread_h(int minor,unsigned int addr)
{
  unsigned int data ;

  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* set up ctrl reg to do a DMA word read, no checks for DSTRT since this interface
   * is very slow!
   */
  /* clear old DMA size,lock flag, proc & periph resets */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_HALFWORD|DRWN|TOF|PRACC ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl=ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* no more transmissions */
  ejtag_table[minor].ctrl &= ~DSTRT;
  
  /* read back data */
  ejt_instr(minor,JTAG_DATA_IR);
  data = ejt_data(minor,0);

  /* clear DMACC */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_HALFWORD;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();

  /* handle the bigendian/littleendian */
  if ( ejtag_table[minor].flags&EJTAG_BIGEND )
    if ( addr & 0x2 )
      data = (data&0x0000ffff) ;
    else
      data = (data>>16)&0xffff ;
  else
    if ( addr & 0x2 )
      data = (data>>16)&0xffff ;
    else
      data = (data&0x0000ffff) ;

  return data ;
}

/* scan in the instructions to do a pi word read */
static unsigned int ejt_dmaread_b(int minor,unsigned int addr)
{
  unsigned int  byte_loc ;
  unsigned int data ;

  cli();
  /* scan in address */
  ejt_instr(minor,JTAG_ADDRESS_IR);
  ejt_data(minor,addr);

  /* set up ctrl reg to do a DMA word read, no checks for DSTRT since this interface
   * is very slow!
   */
  /* clear old DMA size,lock flag, proc & periph resets */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|DMAACC|DSTRT|JTS_BYTE|DRWN|TOF|PRACC ;
  ejt_instr(minor,JTAG_CONTROL_IR);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread with ctrl    %08x\n", minor, ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl=ejt_data(minor,ejtag_table[minor].ctrl);
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: dmaread returns ctrl %08x\n", minor, ejtag_table[minor].ctrl);

  /* no more transmissions */
  ejtag_table[minor].ctrl &= ~DSTRT;
  
  /* read back data */
  ejt_instr(minor,JTAG_DATA_IR);
  data = ejt_data(minor,0);

  /* clear DMACC */
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC|JTS_BYTE;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  ejtag_table[minor].ctrl &= 0x00FFFE7F&~(DLOCK|TIF|SYNC|DMAACC|DRWN|DSTRT|PRRST|PERRST|JTAGBRK) ;
  sti();

  /* handle the bigendian/littleendian */
  /* convert offset to byte offset 0123 BE = ~ 3210 LE */
  byte_loc = (ejtag_table[minor].flags&EJTAG_BIGEND?~addr:addr)&0x3 ;
  data = (data>>(8*byte_loc))&0xff;

  return data ;
}

/* scan out the version register */
static unsigned int ejt_version(int minor)
{
  cli();
  ejt_instr(minor,IDCODE);
  sti();
  return ejt_data(minor,0);
}

/* scan out the implementation register */
static unsigned int ejt_implementation(int minor)
{
  cli();
  ejt_instr(minor,IMPCODE);
  sti();
  return ejt_data(minor,0);
}

/* scan out the implementation register */
static unsigned int ejt_checkstatus(int minor)
{
  unsigned ctrl_orig ;
  ctrl_orig=ejtag_table[minor].ctrl ;
  cli();
  /* ensure that w0 are 1 and w1's are 0 */
  ejtag_table[minor].ctrl &= 0x00FFFFFF ;
  ejtag_table[minor].ctrl &= ~(TIF|SYNC|DSTRT|PRRST|PERRST|JTAGBRK) ;
  ejtag_table[minor].ctrl |= PROBEN|PRACC;
  ejt_instr(minor,JTAG_CONTROL_IR);
  ejt_data(minor,ejtag_table[minor].ctrl);
  sti();
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: ctrl_orig %08x\n",minor,ctrl_orig);
  return ejtag_table[minor].ctrl;
}

/* --- low-level port access ----------------------------------- */

#define r_dtr(x)	(parport_read_data(ejtag_table[(x)].dev->port))
#define r_str(x)	(parport_read_status(ejtag_table[(x)].dev->port))
#define w_ctr(x,y)	do { parport_write_control(ejtag_table[(x)].dev->port, (y)); } while (0)
#define w_dtr(x,y)	do { parport_write_data(ejtag_table[(x)].dev->port, (y)); } while (0)

static __inline__ void ejtag_yield (int minor)
{
  if (!parport_yield_blocking (ejtag_table[minor].dev))
    {
      if (current->need_resched)
	schedule ();
    } else
      ejtag_table[minor].irq_missed = 1;
}

static __inline__ void ejtag_schedule(int minor, long timeout)
{
  struct pardevice *dev = ejtag_table[minor].dev;
  register unsigned long int timeslip = (jiffies - dev->time);
  if ((timeslip > dev->timeslice) && (dev->port->waithead != NULL)) {
    ejtag_parport_release(minor);
    ejtag_table[minor].irq_missed = 1;
    schedule_timeout(timeout);
    ejtag_parport_claim(minor);
  } 
  else
    schedule_timeout(timeout);
}

static int ejtag_reset(int minor)
{
  int retval=0;
  ejtag_parport_claim (minor);
  ejt_reset(minor);
  ejtag_parport_release (minor);
  return retval;
}

#define	ejtag_wait(minor)	udelay(EJTAG_WAIT(minor))


static void ejtag_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
  struct ejtag_struct *ejtag_dev = (struct ejtag_struct *) dev_id;
  
  //  if (waitqueue_active (&ejtag_dev->wait_intr_q))
  // wake_up_interruptible(&ejtag_dev->wait_intr_q);
  
  ejtag_dev->irq_detected = 1;
  ejtag_dev->irq_missed = 0;
}

static ssize_t ejtag_write(struct file * file, const char * buf,
			   size_t count, loff_t *ppos)
{
  unsigned int minor = MINOR(file->f_dentry->d_inode->i_rdev);
  unsigned long copy_size;
  struct ejtag_struct *ejtag = &ejtag_table[minor];
  
  if (minor >= EJTAG_NO)
    return -ENXIO;
  if (ejtag->dev == NULL)
    return -ENXIO;
  
  /* Claim Parport or sleep until it becomes available*/
  ejtag_parport_claim (minor);

  printk(KERN_DEBUG "ejtag_write called count %d\n",count);

  /* copy data into the input buffer and update the buffer length */
  copy_size=0;
  while (copy_size<count) {
    copy_size+=ejtag_buf_write(ejtag->bufout,
			       (char*)(buf+copy_size),(int)count-copy_size) ;
    ejtag_buf_loadSrec(minor);
  }
  ejtag_parport_release (minor);
  return (ssize_t)copy_size ;
}

static ssize_t ejtag_read(struct file * file, char * buf,
			  size_t length, loff_t *ppos)
{
  unsigned int minor=MINOR(file->f_dentry->d_inode->i_rdev);
  // struct ejtag_struct *ejtag = &ejtag_table[minor];
  /* ssize_t count = 0; */
  
  ejtag_parport_claim (minor);
  
  /* if there's no data to be read then block waiting */
  //if ( ejtag->ejtag_bufin_len == 0 )
  //  interruptible_sleep_on(&ejtag->wait_oq);
  
  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: read()\n\r",minor);
  ejtag_parport_release (minor);
  
  return length ;
}

static int ejtag_open(struct inode * inode, struct file * filp)
{
  unsigned int minor = MINOR(inode->i_rdev);
  
  if (minor >= EJTAG_NO)
    return -ENXIO;
  if ((EJTAG_F(minor) & EJTAG_EXIST) == 0)
    return -ENXIO;
  if (test_and_set_bit(EJTAG_BUSY_BIT_POS, &EJTAG_F(minor)))
    return -EBUSY;
  
  MOD_INC_USE_COUNT;
  
  ejtag_parport_claim (minor);
  ejtag_parport_release (minor);
  
  ejtag_table[minor].bufin = kmalloc(sizeof(ejtag_bufm_t), GFP_KERNEL);
  ejtag_table[minor].bufout = kmalloc(sizeof(ejtag_bufm_t), GFP_KERNEL);
  ejtag_table[minor].ejtag_probe_mem = kmalloc(PROBE_MEM_SIZE, GFP_KERNEL);
  if (!ejtag_table[minor].bufin||
      !ejtag_table[minor].bufout||
      !ejtag_table[minor].ejtag_probe_mem) {
    MOD_DEC_USE_COUNT;
    return -ENOMEM;
  }
  if ( dbg )
    ejtag_table[minor].debug=1 ;

  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: opened\n",minor);

  ejtag_buf_init(ejtag_table[minor].bufin);
  ejtag_buf_init(ejtag_table[minor].bufout);

  return 0;
}

static int ejtag_release(struct inode * inode, struct file * file)
{
  unsigned int minor = MINOR(inode->i_rdev);
  
  kfree(ejtag_table[minor].bufin);
  kfree(ejtag_table[minor].bufout);
  kfree(ejtag_table[minor].ejtag_probe_mem);
  ejtag_table[minor].bufin = NULL;
  ejtag_table[minor].bufout = NULL;
  ejtag_table[minor].ejtag_probe_mem = NULL ;
  MOD_DEC_USE_COUNT;
  EJTAG_F(minor) &= ~EJTAG_BUSY;

  if ( ejtag_table[minor].debug )
    printk(KERN_DEBUG "ejtag%d: released\n",minor);

  return 0;
}

static int ejtag_ioctl(struct inode *inode, struct file *file,
		       unsigned int cmd, unsigned long arg)
{
  unsigned int minor = MINOR(inode->i_rdev);
  int retval = 0;
  int preval ;
  if ( ejtag_table[minor].debug )
     printk(KERN_DEBUG "ejtag%d: ioctl, cmd: 0x%08x, arg: 0x%lx\n", minor, cmd, arg);
  if (minor >= EJTAG_NO)
    return -ENODEV;
  if ((EJTAG_F(minor) & EJTAG_EXIST) == 0)
    return -ENODEV;
  
  ejtag_parport_claim (minor);
  
  switch ( cmd ) {
  case EJTAG_INIT:
    ejt_reset(minor);
    ejt_tapmove(minor,0,0); /* enter the run-testidle state */
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_RESET()\n\r",minor);
    break ;
  case EJTAG_TAPRESET:
    ejt_tapreset(minor);
    ejt_tapmove(minor,0,0); /* enter the run-testidle state */
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_TAPRESET()\n\r",minor);
    break ;
  case EJTAG_TAPMOVE:
    retval= (int)ejt_tapmove(minor,arg&TMS_BIT_MASK,arg&TDO_BIT_MASK);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_TAPMOVE(tms=%1d,tdo=%1d)\n\r",
	     minor,
	     arg&TMS_BIT_MASK?0:1,
	     arg&TDO_BIT_MASK?0:1
	     );
    break;
  case EJTAG_DATA:
    retval= ejt_data(minor,(int)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_DATA(%08x)\n\r",minor,(int)arg);
    break;
  case EJTAG_INSTR:
    retval= (int)ejt_instr(minor,(char)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_INSTR(%08x)\n\r",minor,(int)arg);
    break;
  case EJTAG_CTRL_REG:
    ejt_instr(minor,JTAG_CONTROL_IR);
    retval= ejt_data(minor,(int)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_CTRL(%08x)\n\r",minor,(int)arg);
    break;
  case EJTAG_ADDR_REG:
    ejt_instr(minor,JTAG_ADDRESS_IR);
    retval= ejt_data(minor,(int)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_ADDRESS(%08x)\n\r",minor,(int)arg);
    break;
  case EJTAG_DATA_REG:
    ejt_instr(minor,JTAG_DATA_IR);
    retval= ejt_data(minor,(int)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_DATA(%08x)=%08x\n\r",minor,(int)arg,retval);
    break;
  case EJTAG_WRITE_WORD:
    retval= ejt_dmawrite_w(minor,*(int*)arg, *((int*)arg+1));
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_WRITE_WORD(addr=%08x,data=%08x)\n\r",minor,*(int*)arg,*((int*)arg+1));
    break;
  case EJTAG_READ_WORD:
    retval= ejt_dmaread_w(minor,*(int*)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_READ_WORD(addr=%08x)=%08x\n\r",minor,*(int*)arg,retval);
    break;
  case EJTAG_WRITE_HWORD:
    retval= ejt_dmawrite_h(minor,*(int*)arg, *((int*)arg+1));
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_WRITE_HWORD(addr=%08x,data=%08x)\n\r",minor,*(int*)arg,*((int*)arg+1));
    break;
  case EJTAG_READ_HWORD:
    retval= ejt_dmaread_h(minor,*(int*)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_READ_HWORD(addr=%08x)=%08x\n\r",minor,*(int*)arg,retval);
    break;
  case EJTAG_WRITE_BYTE:
    retval= ejt_dmawrite_b(minor,*(int*)arg, *((int*)arg+1));
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_WRITE_BYTE(addr=%08x,data=%08x)\n\r",minor,*(int*)arg,*((int*)arg+1));
    break;
  case EJTAG_READ_BYTE:
    retval= ejt_dmaread_b(minor,*(int*)arg);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_READ_BYTE(addr=%08x)=%08x\n\r",minor,*(int*)arg,retval);
    break;
  case EJTAG_IMPLEMENTATION:
    retval= ejt_implementation(minor);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_IMPLEMENTATION()=%08x\n\r",minor,retval);
    break;
  case EJTAG_VERSION:
    retval= ejt_version(minor);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_VERSION()=%08x\n\r",minor,retval);
    break;
  case EJTAG_CHECKSTATUS:
    preval= ejtag_table[minor].ctrl ;
    retval= ejt_checkstatus(minor);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_CHECKSTATUS()=%08x, preval=%08x\n\r",minor,retval,preval);
    break;
  case EJTAG_PORTWRITE:
    retval= 0 ;
    pp_write_data(minor,arg&0xFF);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_PORTWRITE(%08x)\n\r",minor,(unsigned char)(arg&0xFF));
    break;
  case EJTAG_PORTREAD:
    retval= pp_read_stat(minor);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_PORTREAD()=%08x\n\r",minor,(unsigned char)(retval));
    break;
  case EJTAG_BIGENDIAN:
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_BIGENDIAN()=%08x\n\r",minor,(int)arg);
    if (arg)
      ejtag_table[minor].flags |= EJTAG_BIGEND ;
    else
      ejtag_table[minor].flags &= ~EJTAG_BIGEND ;
    break;
  case EJTAG_WRITE_DATAP:
    retval=0;
    pp_write_data(minor,arg&0xff);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_WRITE_DATAP(%08x)\n\r",minor,(unsigned char)(arg&0xFF));
    break;
  case EJTAG_WRITE_CTRLP:
    retval=0;
    pp_write_ctrl(minor,arg&0xff);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_WRITE_CTRLP(%08x)\n\r",minor,(unsigned char)(arg&0xFF));
    break;
  case EJTAG_READ_STATP:
    retval= pp_read_stat(minor);
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl EJTAG_READ_STATP()=%08x\n\r",minor,(unsigned char)(retval));
    break;
  default:
    if ( ejtag_table[minor].debug )
      printk(KERN_DEBUG "ejtag%d: ioctl, invalid cmd: 0x%08x, arg: 0x%lx\n", minor, cmd, arg);
    retval = -EINVAL;
  }
  ejtag_parport_release (minor);
  return retval;
}

static struct file_operations ejtag_fops = {
  owner:	THIS_MODULE,
  read :        ejtag_read,
  write:        ejtag_write,
  ioctl:        ejtag_ioctl,
  open:         ejtag_open,
  release:      ejtag_release
};

/* --- initialisation code ------------------------------------- */

static int parport_nr[EJTAG_NO] = { [0 ... EJTAG_NO-1] = EJTAG_PARPORT_UNSPEC };
static char *parport[EJTAG_NO] = { NULL,  };
static int reset = 0;
static int dbg=0;

MODULE_PARM(parport, "1-" __MODULE_STRING(EJTAG_NO) "s");
MODULE_PARM(reset, "i");
MODULE_PARM(dbg, "i");


static int ejtag_register(int nr, struct parport *port)
{
    char name[8];
    
    ejtag_table[nr].dev = parport_register_device(port, "ejtag", 
                                                  NULL, NULL,NULL,0,
                                                  (void *) &ejtag_table[nr]);
    if (ejtag_table[nr].dev == NULL)
        return 1;
    ejtag_table[nr].flags |= EJTAG_EXIST;
    
    if (reset)
        ejtag_reset(nr);
    
    printk(KERN_INFO "ejtag%d: using %s (%s).\n", nr, port->name, 
           (port->irq == PARPORT_IRQ_NONE)?"polling":"interrupt-driven");
    
    sprintf (name, "%d", nr);
    devfs_register (devfs_handle, name,
                    DEVFS_FL_DEFAULT, EJTAG_MAJOR, nr,
                    S_IFCHR | S_IRUGO | S_IWUGO,
                    &ejtag_fops, NULL);
    return 0;
}

static void ejtag_attach (struct parport *port)
{
    unsigned int i;
    
    switch (parport_nr[0])
    {
    case EJTAG_PARPORT_UNSPEC:
    case EJTAG_PARPORT_AUTO:
        if (parport_nr[0] == EJTAG_PARPORT_AUTO &&
            port->probe_info[0].class != PARPORT_CLASS_PRINTER)
            return;
        if (ejtag_count == EJTAG_NO) {
            printk("lp: ignoring parallel port (max. %d)\n",EJTAG_NO);
            return;
        }
        if (!ejtag_register(ejtag_count, port))
            ejtag_count++;
        break;
        
    default:
        for (i = 0; i < EJTAG_NO; i++) {
            if (port->number == parport_nr[i]) {
                if (!ejtag_register(i, port))
                    ejtag_count++;
                break;
            }
        }
        break;
    }
}

static void ejtag_detach (struct parport *port)
{
	/* Write this some day. */
}

static struct parport_driver ejtag_driver = {
	"ejtag",
	ejtag_attach,
	ejtag_detach,
	NULL
};

int __init ejtag_init (void)
{
    if (parport_nr[0] == EJTAG_PARPORT_OFF)
        return 0;
    
    if (devfs_register_chrdev (EJTAG_MAJOR, "ejtag", &ejtag_fops)) {
        printk ("ejtag: unable to get major %d\n", EJTAG_MAJOR);
        return -EIO;
    }
    
    devfs_handle = devfs_mk_dir (NULL, "printers", NULL);
    
    if (parport_register_driver (&ejtag_driver)) {
        printk ("ejtag: unable to register with parport\n");
        return -EIO;
    }
    
    if (!ejtag_count) {
        printk (KERN_INFO "ejtag: driver loaded but no devices found\n");
#ifndef CONFIG_PARPORT_1284
        if (parport_nr[0] == EJTAG_PARPORT_AUTO)
            printk (KERN_INFO "ejtag: (is IEEE 1284 support enabled?)\n");
#endif
    }
    
    return 0;
}

static int __init ejtag_init_module (void)
{
    if (parport[0]) {
        /* The user gave some parameters.  Let's see what they were.  */
        if (!strncmp(parport[0], "auto", 4))
            parport_nr[0] = EJTAG_PARPORT_AUTO;
        else {
            int n;
            for (n = 0; n < EJTAG_NO && parport[n]; n++) {
                if (!strncmp(parport[n], "none", 4))
                    parport_nr[n] = EJTAG_PARPORT_NONE;
                else {
                    char *ep;
                    unsigned long r = simple_strtoul(parport[n], &ep, 0);
                    if (ep != parport[n]) 
                        parport_nr[n] = r;
                    else {
                        printk(KERN_ERR "ejtag: bad port specifier `%s'\n", parport[n]);
                        return -ENODEV;
                    }
                }
            }
        }
    }
    
    return ejtag_init();
}

static void ejtag_cleanup_module (void)
{
    unsigned int offset;
    
    parport_unregister_driver (&ejtag_driver);
    
#ifdef CONFIG_EJTAG_CONSOLE
    unregister_console (&ejtagcons);
#endif
    
    devfs_unregister (devfs_handle);
    devfs_unregister_chrdev(EJTAG_MAJOR, "ejtag");
    for (offset = 0; offset < EJTAG_NO; offset++) {
        if (ejtag_table[offset].dev == NULL)
            continue;
        parport_unregister_device(ejtag_table[offset].dev);
    }
}

__setup("ejtag=", ejtag_setup);
module_init(ejtag_init_module);
module_exit(ejtag_cleanup_module);
