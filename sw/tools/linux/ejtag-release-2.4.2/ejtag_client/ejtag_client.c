/* 
 * author padraigo
 * synopsis
 *  simple series of ioctl's to verify the operation of the ejtag driver.
 */

#include <asm/semaphore.h>
#include <sys/ioctl.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <ejtag.h>

#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "ejtag_primitives.h"
#include "remote-mips-ejtag.h"
#include "tm.h"

struct bp {
  struct bp* next;
  unsigned long addr ;
  unsigned long insn ;
};

void info(void);
void identify_part(int fd);
void instr_status(int fd);
void issue_reset(int fd);
void do_jtagbrk(int fd);
void probe_memrd(int fd, int data);
void dumpmem(int fd);
void dumpregs(int fd);
void do_read(int fd);
void do_write(int fd);
void select_dma(void);
void insert_bp(int fd, int addr);
void remove_bp(int fd, int addr);
int  download_binary(int fd,int addr,int bytes,char *fname);

int fd ;
extern unsigned int big_probe_mem[];
int display = 1 ;
unsigned int c = 'q'  ;
unsigned use_dma=0;

char *regnames[]=MIPS_REGISTER_NAMES;

static struct _mips_stuff {
  unsigned long version, implementation ;
  unsigned long cp0_debug, cp0_depc ;
  unsigned  open : 1 ;
  unsigned  single_stepping :1 ;
  unsigned  data_changed :1 ;
  unsigned  code_changed :1 ;
  unsigned long impl, dev_id ;
  int fd ;
  struct bp* pbp;
} mips_data ;

int main(int argc, char **argv)
{
  unsigned int i ;
  int dev_id, impl;
  int addr,data ;
#ifdef BB_DEBUG_BOARD
  char fname[256];
#endif

  fd = open("/dev/ejtag0", O_RDWR|O_SYNC);
  memset(&mips_data,0,sizeof(mips_data));
  if ( fd < 0 )
    {
      perror("Cannot open /dev/ejtag0");
      close(fd);
      exit(1);
    }
  for(;;)
    {
      c=fgetc(stdin);
      if ( isspace(c) ) continue ;

      switch(c)
	{
#ifdef BB_DEBUG_BOARD
        case 'z':
            mips_ejtag_jtagbrk(fd);
            break;
        case 'a':
            printf("JTAG addr = 0x%08x\n",mips_ejtag_getaddr(fd));
            break;
	case 'l':
	  scanf("%x %x %s", &addr, &data, fname);
	  if(download_binary(fd,addr,data,fname))
              printf("Failed to download binary!\n");
          else
              printf("Download complete.\n");
	  break;
        case 't': // tr => tap read, tw <%x> => tap write hex data %x
            c=fgetc(stdin);
            if(c=='w'){
                scanf("%x",&data);
                mips_ejtag_pracc_notdbg(fd,data);
            }
            else{
                printf("processor wrote %08x\n",mips_ejtag_pwacc(fd));
            }
            printf("JTAG addr = 0x%08x\n",mips_ejtag_getaddr(fd));
            break;

#endif
	case 'b':
	  scanf("%x", &addr);
	  insert_bp(fd,addr);
	  break;
	case 'B':
	  scanf("%x", &addr);
	  remove_bp(fd,addr);
	  break;

	  // gdb like commands
	case 'g':
	  dumpregs(fd);
	  break;

	case 'P':
	  scanf("%d %x", &addr, &data);
	  mips_ejtag_setreg(fd,addr,data);
	  break;

	case 'm':
	  dumpmem(fd);
	  break ;

	case 's':
	  if ( ! mips_data.single_stepping ) {
	    mips_ejtag_setSingleStep(mips_data.fd,1);
	  }
	  /* send cpu on its merry way */
	  mips_ejtag_release(mips_data.fd);
	  break;

	case 'c':
	  mips_ejtag_release(fd);
	  break;

	case 'C':
	  mips_ejtag_release(fd);
	  mips_ejtag_wait(fd,&data);
	  break;
	  // native commands
	case 'd':
	  select_dma();
	  break;
	case 'h':
	  info();
	  break;
	case 'i':
	  mips_ejtag_init(fd,&mips_data.dev_id, &mips_data.impl);
	  break;
	case 'p':
	  identify_part(fd);
	  printf( "PRID %08x\n", mips_ejtag_getcp0reg(fd, CP0_PRID));
	  break;
	case 'r':
	  do_read(fd);
	  break ;
	case 'S':
	  instr_status(fd);
	  break ;
	case 'w':
	  do_write(fd);
	  break;
	case 'x':
	  exit(1);
	  break;
	default:
	  printf("wrong selection: %c\n",c);
	};
    }
  close(fd);
}


void info(void)
{
  printf("\n\ndo you want to:\n");
  printf("\n\n\tgdb serial commands:\n");
  printf("\tg get registers\n");
  printf("\tP <%%d> <%%x> set register <%%d> to <%%x>\n");
  printf("\tm <%%x> <%%d> dump <%%d> bytes of memory starting at <%%x>\n");
  printf("\ts single step\n");
  printf("\tc continue\n");
  printf("\n\n\tnative commands:\n");
  printf("\td <%%c> <%%c>==y use ejtag dma, ==n use cpu to read/write memory\n");
  printf("\th print this screen\n");
  printf("\ti init - must be called to initialise communication with device\n");
  printf("\tp print part\n");
  printf("\tr <%%c>={w|h|b} a:<%%x> read word|halfword|byte from <%%x>\n");
  printf("\tw <%%c>={w|h|b} a:<%%x> d:<%%x> write d:<%%x> word|halfword|byte to a:<%%x>\n");
  printf("\tS print the control register status\n");
  printf("\tx exit\n");
}

void select_dma(void)
{
  printf("used dma? [y/n]\n");
  do {
    c = getchar();
  }
  while ( !isspace(c) );
  scanf("%c",&c);
  switch(c){
  case 'Y':
  case 'y':
    use_dma=1;
    break;
  case 'N':
  case 'n':
    use_dma=0;
    break;
  default:
    printf("wrong selection %c\n", c);
  }
}

void issue_reset(int fd)
{
  unsigned ctrl ;
  ctrl = mips_ejtag_checkstatus(fd);
  ctrl = ctrl | PRRST | PERRST ;
  mips_ejtag_instr(fd,JTAG_CONTROL_IR);
  mips_ejtag_data(fd,ctrl);
  ctrl = ctrl & ~(PRRST|PERRST) ;
  mips_ejtag_data(fd,ctrl);
}

void print_ctrl(int ctrl)
{
  printf ("\nCtrl + : %x\n", ctrl);
  printf ("     |\n");
  printf ("     +-- DCLKEN    : %d\n", (ctrl>> 0)&0x1 );
  printf ("     +-- TOF       : %d\n", (ctrl>> 1)&0x1 );
  printf ("     +-- TIF       : %d\n", (ctrl>> 2)&0x1 );
  printf ("     +-- BrkSt     : %d\n", (ctrl>> 3)&0x1 );
  printf ("     +-- Dinc      : %d\n", (ctrl>> 4)&0x1 );
  printf ("     +-- Dlock     : %d\n", (ctrl>> 5)&0x1 );
  printf ("     +-- Dsz       : %d\n", (ctrl>> 7)&0x3 );
  printf ("     +-- Drwm      : %d\n", (ctrl>> 9)&0x1 );
  printf ("     +-- Derr      : %d\n", (ctrl>>10)&0x1 );
  printf ("     +-- Dstrt     : %d\n", (ctrl>>11)&0x1 );
  printf ("     +-- JtagBrk   : %d\n", (ctrl>>12)&0x1 );
  printf ("     +-- DEV       : %d\n", (ctrl>>14)&0x1 );
  printf ("     +-- ProbEn    : %d\n", (ctrl>>15)&0x1 );
  printf ("     +-- PrRst     : %d\n", (ctrl>>16)&0x1 );
  printf ("     +-- DmaAcc    : %d\n", (ctrl>>17)&0x1 );
  printf ("     +-- PrAcc     : %d\n", (ctrl>>18)&0x1 );
  printf ("     +-- PRnW      : %d\n", (ctrl>>19)&0x1 );
  printf ("     +-- PerRst    : %d\n", (ctrl>>20)&0x1 );
  printf ("     +-- Run       : %d\n", (ctrl>>21)&0x1 );
  printf ("     +-- Doze      : %d\n", (ctrl>>22)&0x1 );
  printf ("     +-- Sync      : %d\n", (ctrl>>23)&0x1 );
  printf ("     +-- DsuRst    : %d\n", (ctrl>>24)&0x1 );
  printf ("     +-- Psz       : %d\n", (ctrl>>25)&0x3 );
  printf ("     +-- JtagInt   : %d\n", (ctrl>>27)&0x1 );
  printf ("     +-- JtagIntEn : %d\n", (ctrl>>28)&0x1 );
}

void instr_status(int fd)
{
  int ctrl ;
  
  ctrl = mips_ejtag_checkstatus(fd);

  print_ctrl(ctrl);
}

void peekpoke(int fd)
{
  char c ;
  unsigned addr, data ;

  printf("do you want to\n\t1/ peek \n\t2/ poke\n");
  do {
    c = getchar();
  }
  while (isspace(c));

  switch(c)
    {
    case '1':
      printf("\nEnter addr in hex: ");
      scanf ("%x",&addr);
      printf("(%x) read %x\n",addr, mips_ejtag_read_w(fd,addr));
      break;
    case '2':
      printf("\nEnter addr in hex: ");
      scanf ("%x",&addr);
      printf("\nEnter data in hex: ");
      scanf ("%x",&data);
      mips_ejtag_write_w(fd,addr,data);
      printf("(%x) written %x\n",addr, mips_ejtag_read_w(fd,addr));
      break;
    default:
      printf("wrong selection\n");
    };
}

void do_read(int fd)
{
  char c ;
  unsigned addr, data ;

  printf("w word\nh halfword\nb byte\n");
  do {
    c = getchar();
  }
  while ( !(c=='w'||c=='h'||c=='b') );

  printf("\naddr: ");
  scanf ("%x",&addr);

  switch(c) {
  case 'w':
    if ( use_dma )
      data=mips_ejtag_read_w(fd,addr);
    else
      data=mips_ejtag_procRead_w(fd,addr);

    printf("(%x) read %x\n",addr, data);
    break;
  case 'h':
    printf("(%x) read %x\n",addr, mips_ejtag_read_h(fd,addr));
    break;
  case 'b':
    printf("(%x) read %x\n",addr, mips_ejtag_read_b(fd,addr));
    break;
  default:
    printf("something went wrong %c\n",c);
  };
}

void do_write(int fd)
{
  char c ;
  unsigned addr, data ;

  printf("w word\nh halfword\nb byte\n");
  do {
    c = getchar();
  }
  while ( !(c=='w'||c=='h'||c=='b') );

  printf("\naddr: ");
  scanf ("%x",&addr);
  printf("\ndata: ");
  scanf ("%x",&data);

  switch(c) {
  case 'w':
    if ( use_dma )
      mips_ejtag_write_w(fd,addr,data);
    else
      mips_ejtag_procWrite_w(fd,addr,data);
    break;
  case 'h':
    mips_ejtag_write_h(fd,addr,data); 
    break;
  case 'b':
    mips_ejtag_write_b(fd,addr,data); 
    break;
  default:
    printf("something went wrong %c\n",c);
  };
  printf("\n(%x) written %x\n",addr, mips_ejtag_read_w(fd,addr&~0x3));
}

void identify_part(int fd)
{
  char * manufacturer ;

  mips_data.impl = mips_ejtag_implementation(fd);

  if ( !mips_data.impl )
      printf(" NOTE: implementation register all zeroes!\n");

  printf ("debug register (bits)        : %d\n", mips_data.impl&1 ? 64:32);
  printf ("break channels               : %d\n", (mips_data.impl>>1) & 0xf );
  printf ("instr addr break implemented : %c\n", (mips_data.impl>>5)&1 ? 'n':'y' );
  printf ("data addr break implemented  : %c\n", (mips_data.impl>>6)&1 ? 'n':'y' );
  printf ("proc bus break implemented   : %c\n", (mips_data.impl>>7)&1 ? 'n':'y' );
  
#ifdef BB_DEBUG_BOARD
  /* AJP: others per spec */
  printf("\n");
  printf("EJtag Version: %d\n", (mips_data.impl>>29));
  printf("Dint Support : %d\n", (mips_data.impl>>24)&1);
  printf("NoDMA bit    : %d\n", (mips_data.impl>>14)&1);
#endif

  mips_data.dev_id = mips_ejtag_version(fd);
  if ( !mips_data.dev_id )
    printf(" NOTE: device identification register all zeroes!\n");

  if (mips_data.dev_id == 0xFFFFFFFF & mips_data.impl == 0xFFFFFFFF )
    {
      printf ("Check cable is connected !!!\n");
      return;
    }
  switch((mips_data.dev_id>>1)&0x3ff)
    {
    case 0x001: 
      manufacturer = "Toshiba";
      break;
    case 0x003: 
      manufacturer = "NEC";
      break;
    case 0x007: 
      manufacturer = "IDT";
      break;
    case 0x00F: 
      manufacturer = "Sony";
      break;
    case 0x010: 
      manufacturer = "NKK";
      break;
    case 0x015: 
      manufacturer = "Philips";
      break;
    default:
      sprintf(manufacturer,"%03x", (mips_data.dev_id>>1)&0x3ff);
      break;
    }
  printf ("Part manufactured by         : %s\n", manufacturer);
  printf ("Part number                  : %d\n", (mips_data.dev_id>>12)&0xFFFF);
  printf ("Part version                 : %d\n", (mips_data.dev_id>>28)&0xF);

}

void dumpmem(int fd)
{
  unsigned int base, top, data, addr ;

  printf("Enter the address number of bytes to dump\n");
  scanf("%x %d",&base,&top);
  top=base+top;

  puts("\n\n");
  for(base=base&~0xF, top=(top+3)&~0xF ;base<top; base=base+4)
    {
      if ( (base&0xF) == 0 )
	printf ("%08x: ",base);
      if ( use_dma ) {
	data=mips_ejtag_read_w(fd,base);
      } else {
	data=mips_ejtag_procRead_w(fd,base);
      }
      printf ("%08x%c",data, (base&0xF)==0xC?'\n':' ');
    }
  puts("\n\n");
}


void probe_memrd(int fd, int data)
{
  unsigned addr ;
  unsigned ctrl ;
  int timeout = 0x20 ;

  while (1){
    addr=mips_ejtag_ctrl(fd,DEV|PROBEN|PRACC);
    if ( addr & PRACC )
      if ( ctrl&PRNW ) {
	printf(__FUNCTION__ " early exit\n");
	return ; /* early exit */
      } else {
	break;
      }
  }

  mips_ejtag_instr(fd,JTAG_ADDRESS_IR);
  addr=mips_ejtag_data(fd,0);

  mips_ejtag_instr(fd,JTAG_DATA_IR);
  mips_ejtag_data(fd,data);
  mips_ejtag_instr(fd,JTAG_CONTROL_IR);
  mips_ejtag_data(fd,DEV|PROBEN);

  printf (__FUNCTION__ " %08x : %08x\n", addr,data);
}

int probe_memwr(int fd)
{
  unsigned addr ;
  unsigned data ;
  unsigned ctrl ;

  while (1){
    ctrl=mips_ejtag_ctrl(fd,DEV|PROBEN|PRACC);
    mips_ejtag_instr(fd,JTAG_ADDRESS_IR);
    addr=mips_ejtag_data(fd,0);

    if ( ctrl & PRACC )
      if ( ctrl&PRNW )
	break ;
      else {
	printf(__FUNCTION__ "ADDR: %08x NOP\n",addr);
	mips_ejtag_instr(fd,JTAG_DATA_IR);
	mips_ejtag_data(fd,NOP);
	mips_ejtag_instr(fd,JTAG_CONTROL_IR);
	mips_ejtag_data(fd,DEV|PROBEN);
      }
  }

  mips_ejtag_instr(fd,JTAG_ADDRESS_IR);
  addr=mips_ejtag_data(fd,0);
  printf("CPU writing probe %08x\n",addr);

  //  printf ("JTAG_ADDR  %08x\n", addr);
  //  printf ("Scan %08x mips instr into JTAG_DATA\n",data);
  mips_ejtag_instr(fd,JTAG_DATA_IR);
  data=mips_ejtag_data(fd,data);
  mips_ejtag_instr(fd,JTAG_CONTROL_IR);
  mips_ejtag_data(fd,DEV|PROBEN);

  // printf ("%08x : %08x\n", addr,data);
  return data;
}

// test ejtag/dsu jtagbrk operation, probe memory read and dsu enabling 
// last effect is used in the subsequent real time pc trace
void do_jtagbrk(int fd)
{
  unsigned ctrl ;
  // test jtag break
  //  printf ("\n**** Test JTAG Break ****\n");
  //  printf (" Scan PROBEN|JTAGBRK|PRACC into JTAG_CTRL");
  fflush(stdout);

  printf("jtag break\n");

  // these instructions will not be executed by the cpu from
  // probe memory
  mips_ejtag_pracc(fd,0x4002b000);   // 00000000: 4002b000     mfc0    $v0,$22      
  mips_ejtag_pracc(fd,NOP);          // 00000004: 00000000     nop                  
  mips_ejtag_pracc(fd,0x3c030002);   // 00000010: 3c030002     lui     $v1,2        
  mips_ejtag_pracc(fd,0x00431025);   // 00000014: 00431025     or      $v0,$v0,$v1  
  mips_ejtag_pracc(fd,0x4082b000);   // 0000001c: 4082b000     mtc0    $v0,$22      
  mips_ejtag_pracc(fd,NOP);          // 00000020: 00000000     nop                  
  mips_ejtag_release(fd);
}

void dumpregs(int fd)
{
  int i ;
  int reg ;

  
  fflush(stdout);
  for(i=0;i<=PC_REGNUM;i++) {
    if ( i && !(i%4) ) printf("\n");
    reg=mips_ejtag_getreg(fd,i);      // sw      $i,0($3)
    printf("%5s (%3d): %08x ", regnames[i], i, reg);
  }
  printf("\n");
  for(i=FIRST_EMBED_REGNUM;i<=LAST_EMBED_REGNUM;i++){
    if ( i-FIRST_EMBED_REGNUM && !((i-FIRST_EMBED_REGNUM)%4) ) printf("\n");
    reg=mips_ejtag_getreg(fd,i);      // sw      $i,0($3)
    printf("%5s (%3d): %08x ", regnames[i], i, reg);
  }
  printf("\n");
}

struct bp* locate_bp(int addr)
{
  struct bp* pbp;

  pbp=mips_data.pbp;
  while(pbp!=0&&pbp->addr!=addr)
    pbp=pbp->next;
  return pbp;
}

void insert_bp(int fd, int addr)
{
  struct bp* pbp;

  pbp=locate_bp(addr);
  if ( pbp ) {
    fprintf(stderr,"addr %08x already has a breakpoint",addr);
  } else {
    // allocate space for breakpoint
    pbp=malloc(sizeof(struct bp));
    assert(pbp);
    // update bp data structure
    pbp->addr=addr;
    pbp->insn=mips_ejtag_procRead_w(fd,addr);
    // write breakpoint instruction into memory
    mips_ejtag_procWrite_w(fd,addr,SDBBP(0));
    // add bp to list
    pbp->next=mips_data.pbp;
    mips_data.pbp=pbp;
  }
}

void remove_bp(int fd, int addr)
{
  struct bp* pbp, *pbp_prev;

  pbp=mips_data.pbp;
  while(pbp!=0&&pbp->addr!=addr) {
    pbp_prev=pbp; 
    pbp=pbp->next;
  }
  if ( !pbp ) {
    fprintf(stderr,"addr %08x already has a breakpoint",addr);
  } else {
    pbp_prev->next=pbp->next;
    mips_ejtag_procWrite_w(fd,pbp->addr,pbp->insn);
    free(pbp);
  }
}

#define Pio_BaseAddress				0xA001A000	/* Base Address */
#define Pio_InputRegister			0x00		/* (PIO_IN)         Input Register */
#define Pio_OutputRegister			0x04		/* (PIO_OUT)        Output Register */
#define Pio_DirectionRegister			0x08		/* (PIO_DIR)        Direction Register */
#define Pio_SelectRegister			0x14		/* (PIO_SEL)        Select Register */

#ifdef BB_DEBUG_BOARD
int download_binary(int fd,int addr,int bytes,char *fname)
{
    FILE *fp;
    int val, count=0;

    fp = fopen(fname,"r");
    if(!fp)
        return -1;

    if(bytes==0)bytes=-1;
    while(bytes && fread(&val,4,1,fp)){
        mips_ejtag_procWrite_w(fd,addr,val);
        if(!(count&(1024-1)))
            printf("addr = %08x\n");
        count+=4;
        addr+=4;
        if(bytes>0){
            bytes-=4;
            if(bytes<0)bytes=0;
        }
    }

    fclose(fp);
    return 0;
}
#endif
