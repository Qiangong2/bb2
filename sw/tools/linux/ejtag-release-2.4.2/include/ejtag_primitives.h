/*
 * MIPS EJTAG Target
 * Copyright (C) 2001 Padraig O Mathuna (padraigo@yahoo.com)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _EJTAG_PRIMITIVES_H_
#define _EJTAG_PRIMITIVES_H_

/* ejtag primitives */
int mips_ejtag_init (int fd,unsigned long* dev_id, unsigned long*impl);
int mips_ejtag_instr(int fd,char instr);
int mips_ejtag_implementation(int fd);
int mips_ejtag_version(int fd);
int mips_ejtag_data(int fd,int data);
int mips_ejtag_write_w(int fd,unsigned int addr, unsigned int data);
int mips_ejtag_read_w(int fd,unsigned int addr);
int mips_ejtag_write_h(int fd,unsigned int addr, unsigned int data);
int mips_ejtag_read_h(int fd,unsigned int addr);
int mips_ejtag_write_b(int fd,unsigned int addr, unsigned int data);
int mips_ejtag_read_b(int fd,unsigned int addr);
int mips_ejtag_ctrl(int fd, unsigned ctrl);
int mips_ejtag_checkstatus(int fd);
int mips_ejtag_portwrite(int fd,unsigned char byte);
int mips_ejtag_portread(int fd);
void mips_ejtag_pracc(int fd, int data);
void mips_ejtag_jtagbrk(int fd);
void mips_ejtag_setDSU(int fd,int en);
void mips_ejtag_release(int fd);
void mips_ejtag_setreg(int fd, int regnum, int val);
int mips_ejtag_getreg(int fd, int regnum);
unsigned int mips_ejtag_returnWord(int fd,unsigned int word, unsigned int addr);
unsigned int mips_ejtag_forwardWord(int fd, unsigned int addr);
void mips_ejtag_setSingleStep(int fd, int dss);
int mips_ejtag_toggleEJTAGWrites(int fd);
void mips_ejtag_ctrldump(int fd);
void mips_ejtag_dumpRegisters(int fd);
void mips_ejtag_storev0v1(int fd,unsigned long v[]);
void mips_ejtag_restorev0v1(int fd,unsigned long v[]);
void mips_ejtag_flushICache(int fd);
void mips_ejtag_flushDCache(int fd);
void mips_ejtag_procWrite_w(int fd, unsigned long addr, unsigned long data);
unsigned long mips_ejtag_procRead_w(int fd, unsigned long addr);
int mips_ejtag_wait (int fd,unsigned int *pstat);
#ifdef BB_DEBUG_BOARD
unsigned int mips_ejtag_pwacc(int fd);
void mips_ejtag_pracc_notdbg(int fd, int data);
#endif

#define DEBUG_FUNC_CALL  (1<<0)
#define DEBUG_REG_ACCESS (1<<1)
#define DEBUG_MEM_ACCESS (1<<2)
#define DEBUG_PRACC      (1<<3)
#define DEBUG_EXCEPTION  (1<<4)

extern int ejtag_debug ;

#endif /* _EJTAG_PRIMITIVES_H_ */
